class ChatModel {
  int id;
  String idUser;
  String chatText;
  String date;
  String isParent;

  ChatModel({
    required this.id,
    required this.idUser,
    required this.chatText,
    required this.date,
    required this.isParent,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "idUser": idUser,
      "chatText": chatText,
      "date": date,
      "isParent": isParent
    };
  }
}
