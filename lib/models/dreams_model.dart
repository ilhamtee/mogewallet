class DreamsModel {
  int? id;
  String namaImpian;
  String tanggalPencapaian;
  String danaPencapaian;
  String danaAwal;
  String danaTerkumpul;
  String tabunganOtomatis;
  String nominalTabunganOtomatis;
  String imageDummy;
  String imagePath;
  String isKunciTglPencapaian;
  String isActive;

  DreamsModel({
    this.id,
    required this.namaImpian,
    required this.tanggalPencapaian,
    required this.danaPencapaian,
    required this.danaAwal,
    required this.danaTerkumpul,
    required this.tabunganOtomatis,
    required this.nominalTabunganOtomatis,
    required this.imageDummy,
    required this.imagePath,
    required this.isKunciTglPencapaian,
    required this.isActive,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "namaImpian": namaImpian,
      "tanggalPencapaian": tanggalPencapaian,
      "danaPencapaian": danaPencapaian,
      "danaAwal": danaAwal,
      "danaTerkumpul": danaTerkumpul,
      "tabunganOtomatis": tabunganOtomatis,
      "nominalTabunganOtomatis": nominalTabunganOtomatis,
      "imageDummy": imageDummy,
      "imagePath": imagePath,
      "isKunciTglPencapaian": isKunciTglPencapaian,
      "isActive": isActive
    };
  }
}
