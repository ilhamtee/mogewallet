class SaldoMomsWalletModel {
  int id;
  String saldo;

  SaldoMomsWalletModel({
    required this.id,
    required this.saldo,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "saldo": saldo,
    };
  }
}
