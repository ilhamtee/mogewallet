import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/models/user_model.dart';
import 'package:moge_wallet/ui/pages/menu/dashboard/dashboard_page.dart';
import 'package:moge_wallet/ui/pages/menu/history/history_page.dart';
import 'package:moge_wallet/ui/pages/menu/income/income_page.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/model/rules_model.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/moms_wallet_page.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/money_saving_page.dart';
import 'package:moge_wallet/ui/pages/menu/outcome/outcome_page.dart';
import 'package:moge_wallet/ui/pages/menu/reminder/reminder_page.dart';
import 'package:moge_wallet/ui/pages/menu/setting/setting_page.dart';
import 'package:moge_wallet/ui/pages/scan_qr/scanQR_page.dart';
import 'package:moge_wallet/ui/widgets/widget_menu_drawer.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/services/rules_service.dart';
import 'package:moge_wallet/utils/services/saldo_moms_service.dart';
import 'package:moge_wallet/utils/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  late List<Widget> _listWidgetMenu;
  int _selectedMenu = 0;
  String? _name;
  bool _isParent = false;

  _getProfileUser() async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    String? idUser = _sharedPref.getString(ConstantHelper.ID_USER);

    List<UserModel> listAllUser = await UserService.getDataUser();
    List<UserModel> currentUser =
        listAllUser.where((user) => user.id.toString() == idUser).toList();

    if (currentUser.first.isParent == "1") {
      setState(() {
        _isParent = true;
        _selectedMenu = 5;
      });
    }

    setState(() {
      _name = currentUser.first.nama;
    });
  }

  _setDataRules() async {
    List<RulesModel> listRules = await RulesService.getDataRules();

    if (listRules.length == 0) {
      List<RulesModel> addRules = [
        RulesModel(
            rulesName: "SPP",
            date: "2020-11-14",
            nominal: "450000",
            isPaid: "0"),
        RulesModel(
            rulesName: "KOST",
            date: "2020-11-05",
            nominal: "600000",
            isPaid: "0"),
        RulesModel(
            rulesName: "Uang Makan",
            date: "2020-11-03",
            nominal: "1500000",
            isPaid: "1")
      ];

      addRules.forEach((rules) async {
        await RulesService.addRules(rules);
      });
    }
  }

  _setSaldoMomsWallet() async {
    var dataSaldo = await MomsBalanceService.getAllBalances();

    if (dataSaldo.length == 0) {
      SaldoMomsWalletModel saldoModel = SaldoMomsWalletModel(saldo: "0", id: 0);
      await MomsBalanceService.addSaldo(saldoModel);
    }
  }

  @override
  void initState() {
    _getProfileUser();
    _setDataRules();
    _setSaldoMomsWallet();

    _listWidgetMenu = [
      DashboardPage(
        scaffoldState: _scaffoldKey,
      ),
      IncomePage(
        scaffoldState: _scaffoldKey,
      ),
      OutcomePage(
        scaffoldState: _scaffoldKey,
      ),
      MoneySavingPage(scaffoldState: _scaffoldKey),
      ReminderPage(scaffoldState: _scaffoldKey),
      MomsWalletPage(scaffoldState: _scaffoldKey),
      HistoryPage(scaffoldState: _scaffoldKey),
      SettingPage(scaffoldState: _scaffoldKey),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion(
      value: SystemUiOverlayStyle(
        statusBarColor: MogeColors.primaryColor,
      ),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: MogeColors.lightBlueColor,
        body: Padding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: _listWidgetMenu[_selectedMenu],
                ),
              ),
            ],
          ),
        ),
        drawer: _widgetDrawer(),
      ),
    );
  }

  _widgetDrawer() {
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
      child: Drawer(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: MediaQuery.of(context).size.height,
          color: MogeColors.drawerBackgroundColor,
          child: Stack(
            children: <Widget>[
              ListView(
                children: <Widget>[
                  _headerDrawer(),
                  Divider(
                    color: Colors.white,
                    height: 2,
                    thickness: 2,
                  ),
                  SizedBox(height: 15),
                  WidgetMenuDrawer(
                    title: "Dashboard",
                    icon: "assets/icons/home.svg",
                    onTap: () {
                      setState(() {
                        _selectedMenu = ConstantHelper.INDEX_MENU_DASHBOARD;
                      });
                      Navigator.pop(context);
                    },
                  ),
                  !_isParent
                      ? WidgetMenuDrawer(
                          title: "Income",
                          icon: "assets/icons/wallet.svg",
                          onTap: () {
                            setState(() {
                              _selectedMenu = ConstantHelper.INDEX_MENU_INCOME;
                            });
                            Navigator.pop(context);
                          },
                        )
                      : Container(),
                  !_isParent
                      ? WidgetMenuDrawer(
                          title: "Outcome",
                          icon: "assets/icons/outcome.svg",
                          onTap: () {
                            setState(() {
                              _selectedMenu = ConstantHelper.INDEX_MENU_OUTCOME;
                            });
                            Navigator.pop(context);
                          },
                        )
                      : Container(),
                  !_isParent
                      ? WidgetMenuDrawer(
                          title: "Dreams Saver",
                          icon: "assets/icons/savings.svg",
                          onTap: () {
                            setState(() {
                              _selectedMenu =
                                  ConstantHelper.INDEX_MENU_MONEY_SAVING;
                            });
                            Navigator.pop(context);
                          },
                        )
                      : Container(),
                  !_isParent
                      ? WidgetMenuDrawer(
                          title: "Reminder",
                          icon: "assets/icons/reminder.svg",
                          onTap: () {
                            setState(() {
                              _selectedMenu =
                                  ConstantHelper.INDEX_MENU_REMINDER;
                            });
                            Navigator.pop(context);
                          },
                        )
                      : Container(),
                  WidgetMenuDrawer(
                    title: "Moms Wallet",
                    icon: "assets/icons/mother.svg",
                    onTap: () {
                      setState(() {
                        _selectedMenu = ConstantHelper.INDEX_MENU_MOMS_WALLET;
                      });
                      Navigator.pop(context);
                    },
                  ),
                  !_isParent
                      ? WidgetMenuDrawer(
                          title: "History",
                          icon: "assets/icons/history.svg",
                          onTap: () {
                            setState(() {
                              _selectedMenu = ConstantHelper.INDEX_MENU_HISTORY;
                            });
                            Navigator.pop(context);
                          },
                        )
                      : Container(),
                  WidgetMenuDrawer(
                    title: "Setting",
                    icon: "assets/icons/settings.svg",
                    onTap: () {
                      setState(() {
                        _selectedMenu = ConstantHelper.INDEX_MENU_SETTING;
                      });
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              Positioned(
                bottom: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: InkWell(
                          onTap: () =>
                              NavigateHelpers.push(context, ScanQRPage()),
                          child: SvgPicture.asset(
                            "assets/icons/scan_qr_drawer.svg",
                            colorFilter: ColorFilter.mode(
                              MogeColors.primaryColor,
                              BlendMode.srcIn,
                            ),
                            width: 30,
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _headerDrawer() {
    return Container(
      height: 150,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(
              _isParent
                  ? "assets/icons/mother_drawer.svg"
                  : "assets/icons/boy.svg",
              width: 60),
          SizedBox(
            height: 20,
          ),
          Text(
            _name != null ? "Hai, " + _name!.toUpperCase() : "",
            style: TextStyle(color: MogeColors.blueColor, fontSize: 20),
          )
        ],
      ),
    );
  }
}
