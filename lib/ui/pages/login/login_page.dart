import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moge_wallet/ui/pages/home/home_page.dart';
import 'package:moge_wallet/ui/pages/register/landing_page.dart';
import 'package:moge_wallet/ui/pages/register/register_page.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/services/login_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/login_model.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _noHandphoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.center,
              child: Text(
                "MOGE",
                style: TextStyle(
                    color: MogeColors.primaryColor,
                    fontSize: 50,
                    fontFamily: "MedioVintage"),
              ),
            ),
          ),
          Container(
            height: 250,
            padding: EdgeInsets.only(bottom: 15, top: 10),
            margin:
                EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _inputNoHandphone(),
                _buttonSignIn(),
                _buttonSignUp(),
              ],
            ),
          )
        ],
      ),
    );
  }

  _buttonSignIn() {
    return InkWell(
      onTap: () {
        if (_noHandphoneController.text.isNotEmpty) {
          _doLogin();
        } else {
          SystemChannels.textInput.invokeMethod('TextInput.hide');
          showFlutterToast("Masukkan Nomor Handphone");
        }
      },
      splashColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        height: 55,
        margin: EdgeInsets.only(right: 60, left: 60),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(color: MogeColors.primaryColor, width: 2),
            borderRadius: BorderRadius.all(Radius.circular(40))),
        child: Text(
          "SIGN IN",
          style: TextStyle(
            color: MogeColors.primaryColor,
            fontSize: 25,
          ),
        ),
      ),
    );
  }

  _buttonSignUp() {
    return InkWell(
      onTap: () {
        _noHandphoneController.clear();
        NavigateHelpers.push(context, RegisterPage());
      },
      splashColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        height: 55,
        margin: EdgeInsets.only(right: 60, left: 60, top: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: MogeColors.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(40))),
        child: Text(
          "SIGN UP",
          style: TextStyle(color: Colors.white, fontSize: 25),
        ),
      ),
    );
  }

  _inputNoHandphone() {
    return Padding(
      padding: EdgeInsets.only(right: 10, left: 10, bottom: 20),
      child: TextField(
        controller: _noHandphoneController,
        style: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 14),
        autofocus: false,
        keyboardType: TextInputType.number,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
        ],
        cursorColor: MogeColors.primaryColor,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(top: 15),
          hintText: "NOMOR PONSEL",
          hintStyle: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 14),
          prefixIcon: Icon(
            Icons.account_circle,
            size: 35,
            color: MogeColors.primaryColor,
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MogeColors.primaryColor),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MogeColors.primaryColor),
          ),
        ),
      ),
    );
  }

  _doLogin() async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    List<LoginModel> _dataUser = await LoginService.getAllUsers();
    List<LoginModel> _validateListUser = _dataUser;

    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    bool? isLogin = sharedPref.getBool(ConstantHelper.IS_LOGIN);

    if (_dataUser.length == 0) {
      showFlutterToast("Silahkan mendaftar terlebih dahulu");
    } else {
      _validateListUser = _dataUser
          .where((data) => data.no_handphone == _noHandphoneController.text)
          .toList();
      if (_validateListUser.length > 0) {
        LoadingDialogWidget.showLoading(context);
        await Future.delayed(Duration(seconds: 2), () async {
          if (isLogin != null) {
            await sharedPref.setString(
                ConstantHelper.ID_USER, _validateListUser.first.id.toString());
            await sharedPref.setString(
                ConstantHelper.USER_NAME, _validateListUser.first.nama);
            NavigateHelpers.pushAndRemove(context, HomePage());
          } else {
            await sharedPref.setString(
                ConstantHelper.ID_USER, _validateListUser.first.id.toString());
            await sharedPref.setString(
                ConstantHelper.USER_NAME, _validateListUser.first.nama);
            await sharedPref.setBool(ConstantHelper.IS_LOGIN, true);
            NavigateHelpers.pushAndRemove(context, LandingPage());
          }
        });
      } else {
        showFlutterToast("Nomor Handphone Salah");
      }
    }
  }
}
