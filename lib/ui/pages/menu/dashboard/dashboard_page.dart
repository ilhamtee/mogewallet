import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/ui/widgets/dashboard/daily_chart_widget.dart';
import 'package:moge_wallet/ui/widgets/dashboard/monthly_chart_widget.dart';
import 'package:moge_wallet/ui/widgets/dashboard/weekly_chart_widget.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';

class DashboardPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  DashboardPage({
    required this.scaffoldState,
  });

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  bool _isDayActive = true;
  bool _isWeekActive = false;
  bool _isMonthActive = false;

  List<HistoryModel> _listOutcome = [];
  String _totalOutCome = "0";

  void _setDataIncomeOutcome() async {
    List<HistoryModel> dataIncome = await IncomeService.getDataIncome();

    if (dataIncome.length == 0) {
      final List<HistoryModel> addIncome = HistoryModel.initialHistoriesModel;

      addIncome.forEach((element) {
        IncomeService.addIncome(element);
      });
    }
  }

  void _countTotalIncome() {
    int total = 1065000;
    if (_listOutcome.length > 0) {
      total = 0;
      _listOutcome.forEach((data) {
        total += int.parse(data.nominal);
      });
    }

    setState(() {
      _totalOutCome = total.toString();
    });
  }

  void _getDataOutcome() async {
    var getDataIncome = await IncomeService.getDataIncome();
    setState(() {
      _listOutcome =
          getDataIncome.where((element) => element.type == "1").toList();
    });
    _countTotalIncome();
  }

  @override
  void initState() {
    _setDataIncomeOutcome();
    _getDataOutcome();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _widgetAppBar(),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.2,
            child: Stack(
              children: [
                _widgetBackgroundHeader(),
                _widgetHeaderDashboard(),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ChoiceChip(
                      label: Text(
                        "Harian",
                        style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      selectedColor: MogeColors.darkBlueColor,
                      selected: _isDayActive,
                      onSelected: (_) {
                        setState(() {
                          _isDayActive = true;
                          _isWeekActive = false;
                          _isMonthActive = false;
                        });
                      },
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    ChoiceChip(
                      label: Text(
                        "Mingguan",
                        style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      selectedColor: MogeColors.darkBlueColor,
                      selected: _isWeekActive,
                      onSelected: (_) {
                        setState(() {
                          _isDayActive = false;
                          _isWeekActive = true;
                          _isMonthActive = false;
                        });
                      },
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    ChoiceChip(
                      label: Text(
                        "Bulanan",
                        style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      selectedColor: MogeColors.darkBlueColor,
                      selected: _isMonthActive,
                      onSelected: (_) {
                        setState(() {
                          _isDayActive = false;
                          _isWeekActive = false;
                          _isMonthActive = true;
                        });
                      },
                    ),
                  ],
                ),
                Container(
                  height: 160,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                  child: _isDayActive
                      ? _widgetDataDays()
                      : _isWeekActive
                          ? _widgetDataWeeks()
                          : _widgetDataMonths(),
                ),
              ],
            ),
          ),
          if (_isDayActive)
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                  right: 16,
                  bottom: 12,
                ),
                child: DailyChartWidget(),
              ),
            )
          else if (_isWeekActive)
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                  bottom: 8,
                ),
                child: WeeklyChartWidget(),
              ),
            )
          else
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: MonthlyChartWidget(),
              ),
            ),
        ],
      ),
    );
  }

  Widget _widgetHeaderDashboard() {
    return Positioned(
      top: 10,
      left: 25,
      right: 25,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 150,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Total Pengeluaran",
                      style: TextStyle(color: MogeColors.primaryColor),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 15, left: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Rp. ${FormatNumber.formatterLocal(_totalOutCome)},-",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  SizedBox(height: 5),
                  Text(
                    "(25%)",
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 15),
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Limit",
                          style: TextStyle(
                              color: MogeColors.primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          " Bulan Ini",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                      ],
                    ),
                    Text(
                      "Rp. 1.800.000-,",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.symmetric(horizontal: 20),
        color: MogeColors.primaryColor,
        child: Stack(
          children: <Widget>[
            Align(
              child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer()),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "MOGE",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  Widget _widgetBackgroundHeader() {
    return Positioned(
      top: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.2,
        color: MogeColors.primaryColor,
      ),
    );
  }

  Widget _widgetDataDays() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(121, 230, 67, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Sangat Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(172, 240, 139, 1)),
                  child: Text(
                    "Rp. 45.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "3",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "${DateFormat('EEE, d MMM', "id_ID").format(DateTime.now())}",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Tidak Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(242, 157, 157, 1)),
                  child: Text(
                    "Rp. 99.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "6",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "${DateFormat('EEE, d MMM', "id_ID").format(DateTime.now().subtract(Duration(days: 1)))}",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(237, 216, 52, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(237, 225, 142, 1)),
                  child: Text(
                    "Rp. 60.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "5",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "${DateFormat('EEE, d MMM', "id_ID").format(DateTime.now().subtract(Duration(days: 2)))}",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _widgetDataWeeks() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(121, 230, 67, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Sangat Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(172, 240, 139, 1)),
                  child: Text(
                    "Rp. 80.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "3",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "Minggu ke-1",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Tidak Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(242, 157, 157, 1)),
                  child: Text(
                    "Rp. 75.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "7",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "Minggu ke-2",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(237, 216, 52, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(237, 225, 142, 1)),
                  child: Text(
                    "Rp. 55.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "8",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "Minggu ke-3",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _widgetDataMonths() {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(121, 230, 67, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Sangat Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(172, 240, 139, 1)),
                  child: Text(
                    "Rp. 45.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "1",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "November",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(right: 10),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Tidak Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(242, 157, 157, 1)),
                  child: Text(
                    "Rp. 88.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "5",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "Desember",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
                color: Color.fromRGBO(237, 216, 52, 1),
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Bagus",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 13),
                ),
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      color: Color.fromRGBO(237, 225, 142, 1)),
                  child: Text(
                    "Rp. 77.000,-",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "7",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 17),
                    ),
                    Text(
                      " Transaksi",
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    )
                  ],
                ),
                Text(
                  "Januari",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
