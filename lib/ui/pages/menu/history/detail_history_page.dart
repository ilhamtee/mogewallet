import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';

class DetailHistoryPage extends StatelessWidget {
  final HistoryModel historyModel;

  DetailHistoryPage({
    required this.historyModel,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            _widgetAppBar(context),
            Padding(
              padding: EdgeInsets.all(10),
              child: historyModel.type == "1"
                  ? _widgetPengeluaran()
                  : _widgetPemasukan(),
            )
          ],
        ),
      ),
    );
  }

  _widgetAppBar(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                historyModel.type == "1" ? "Pengeluaran" : "Pemasukan",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            )
          ],
        ));
  }

  _widgetPemasukan() {
    return ClipRRect(
        child: Container(
      height: 130,
      margin: const EdgeInsets.only(bottom: 6.0, right: 6.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(1.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      "Waktu",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      "Nominal",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            children: List.generate(
                200 ~/ 10,
                (index) => Expanded(
                      child: Container(
                        color: index % 2 == 0
                            ? Colors.transparent
                            : MogeColors.blueColor,
                        height: 2,
                      ),
                    )),
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Center(
                    child: Text(
                      DateTimeHelper.parseDateWithTime(historyModel.date),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: MogeColors.blueColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: Center(
                    child: Text(
                      "Rp " + FormatNumber.formatterLocal(historyModel.nominal),
                      style: TextStyle(
                          color: MogeColors.blueColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    ));
  }

  _widgetPengeluaran() {
    return ClipRRect(
        child: Container(
      height: 150,
      margin: const EdgeInsets.only(bottom: 6.0, right: 6.0),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(1.0, 1.0), //(x,y)
            blurRadius: 6.0,
          ),
        ],
      ),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      "Waktu",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      "Kebutuhan",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  height: 40,
                  child: Center(
                    child: Text(
                      "Nominal",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 5),
          Row(
            children: List.generate(
                200 ~/ 10,
                (index) => Expanded(
                      child: Container(
                        color: index % 2 == 0
                            ? Colors.transparent
                            : MogeColors.blueColor,
                        height: 2,
                      ),
                    )),
          ),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  child: Center(
                    child: Text(
                      DateTimeHelper.parseDateWithTime(historyModel.date),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: MogeColors.blueColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: Center(
                    child: Text(
                      historyModel.nama,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: MogeColors.blueColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  child: Center(
                    child: Text(
                      "Rp " + FormatNumber.formatterLocal(historyModel.nominal),
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    ));
  }
}
