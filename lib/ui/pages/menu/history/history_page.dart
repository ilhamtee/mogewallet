import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:moge_wallet/ui/pages/menu/history/detail_history_page.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';

class HistoryPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  HistoryPage({required this.scaffoldState});

  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  List<HistoryModel>? _listHistory;

  _generateData() async {
    var getAllData = await IncomeService.getDataIncome();
    setState(() {
      _listHistory = getAllData;
    });
  }

  @override
  void initState() {
    _generateData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MogeColors.lightBlueColor,
      child: Column(
        children: <Widget>[
          _widgetAppBar(),
          _listHistory != null
              ? _widgetListHistory()
              : Container(
                  child: Center(
                    child: Text("Belum Ada Riwayat Apapun"),
                  ),
                )
        ],
      ),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer()),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Riwayat",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetListHistory() {
    return Expanded(
      child: Container(
        child: GroupedListView<HistoryModel, String>(
          groupBy: (element) => DateTimeHelper.standardDateFormat(element.date),
          padding: EdgeInsets.only(top: 0),
          elements: _listHistory!,
          shrinkWrap: true,
          sort: false,
          groupSeparatorBuilder: (String value) {
            return Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                DateTimeHelper.dateFormatSeparator(value),
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 15),
              ),
            );
          },
          itemBuilder: (context, element) {
            return InkWell(
              onTap: () {
                NavigateHelpers.push(
                    context, DetailHistoryPage(historyModel: element));
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                height: 80,
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        element.type == "1" ? "Pengeluaran" : "Pemasukan",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        element.type == "1"
                            ? "- Rp " +
                                FormatNumber.formatterLocal(element.nominal)
                            : "Rp " +
                                FormatNumber.formatterLocal(element.nominal),
                        style: TextStyle(
                            color: element.type == "1"
                                ? Colors.red
                                : MogeColors.primaryColor,
                            fontSize: 17,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
