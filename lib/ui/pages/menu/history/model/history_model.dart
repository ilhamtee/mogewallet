class HistoryModel {
  int? id;
  String nama;
  String date;
  String nominal;
  String jenisRiwayat;
  String type;

  HistoryModel({
    this.id,
    required this.nama,
    required this.date,
    required this.nominal,
    this.jenisRiwayat = "",
    required this.type,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "nama": nama,
      "date": date,
      "nominal": nominal,
      "jenisRiwayat": jenisRiwayat,
      "type": type
    };
  }

  static List<HistoryModel> initialHistoriesModel = [
    HistoryModel(
        nama: "Uang Jajan",
        date: "2020-12-19 10:23",
        nominal: "170000",
        type: "0",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Top Up ATM",
        date: "2020-12-19 18:27",
        nominal: "55000",
        type: "0",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Uang Bulanan",
        date: "2020-12-30 06:12",
        nominal: "1500000",
        type: "0",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Bayar SPP",
        date: "2020-12-30 07:05",
        nominal: "450000",
        type: "1",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Bayar Kost",
        date: "2020-12-30 10:45",
        nominal: "600000",
        type: "1",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Top Up ATM",
        date: "2021-01-03 20:30",
        nominal: "70000",
        type: "0",
        jenisRiwayat: ""),
    HistoryModel(
        nama: "Uang Makan",
        date: "2021-01-03 21:17",
        nominal: "15000",
        type: "1",
        jenisRiwayat: ""),
  ];
}
