import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';

class IncomePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  IncomePage({
    required this.scaffoldState,
  });

  @override
  _IncomePageState createState() => _IncomePageState();
}

class _IncomePageState extends State<IncomePage> {
  List<HistoryModel> _listIncome = [];

  TextEditingController _namaPemasukanController = TextEditingController();
  TextEditingController _nominalPemasukanController = TextEditingController();
  String _totalIncome = "0";

  _countTotalIncome() {
    int total = 0;
    if (_listIncome.length > 0) {
      _listIncome.forEach((data) {
        total += int.parse(data.nominal);
      });
    }

    setState(() {
      _totalIncome = total.toString();
    });
  }

  getDataIncome() async {
    
    var getDataIncome = await IncomeService.getDataIncome();
    setState(() {
      _listIncome = getDataIncome
          .where((element) =>
              element.type == "0" ||
              element.type == "3" &&
                  DateFormat("MM").format(DateTime.parse(element.date)) ==
                      DateFormat("MM").format(DateTime.now()))
          .toList();
    });
    _countTotalIncome();
  }

  @override
  void initState() {
    getDataIncome();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      appBar: _widgetAppBar(),
      body: Container(
        color: MogeColors.lightBlueColor,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              color: Color.fromRGBO(206, 221, 242, 1),
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  Text(
                    "Total Pemasukan Bulan ${DateFormat("MMMM", "id").format(DateTime.now())} (Rp)",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.w400,
                        fontSize: 13),
                  ),
                  SizedBox(height: 5),
                  Text(
                    FormatNumber.formatterLocal(_totalIncome),
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 25),
                  ),
                ],
              ),
            ),
            _listIncome.length > 0
                ? Expanded(
                    child: Container(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listIncome.length,
                          itemBuilder: (context, index) {
                            HistoryModel data = _listIncome[index];
                            return _widgetItemIncome(data);
                          }),
                    ),
                  )
                : Expanded(
                    child: Center(
                    child: Text(
                      "Belum Ada Pemasukan",
                      style: TextStyle(color: MogeColors.primaryColor),
                    ),
                  ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: MogeColors.primaryColor,
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 40,
        ),
        onPressed: () async {
          _showDialogAddIncome();
        },
      ),
    );
  }

  _widgetItemIncome(HistoryModel data) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          border:
              Border.all(width: 2.0, color: Color.fromRGBO(206, 221, 242, 1)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data.nama,
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 15),
                  ),
                  SizedBox(height: 5),
                  Text(
                    DateTimeHelper.dateParseLocalSeparator(data.date),
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 12),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.green),
                      ),
                      SizedBox(width: 5),
                      Text(
                        "Pemasukan ",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontWeight: FontWeight.w500,
                            fontSize: 15),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    FormatNumber.formatter(data.nominal),
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 15),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _widgetAppBar() {
    return PreferredSize(
        preferredSize: Size.fromHeight(45.0), // here the desired height
        child: AppBar(
          backgroundColor: MogeColors.primaryColor,
          centerTitle: true,
          elevation: 0,
          leading: Padding(
            padding: EdgeInsets.only(bottom: 15, left: 25),
            child: InkWell(
              onTap: () => widget.scaffoldState.currentState?.openDrawer(),
              child: Icon(
                Icons.menu,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 15, right: 25),
              child: InkWell(
                  child: SvgPicture.asset(
                "assets/icons/filter.svg",
                colorFilter: ColorFilter.mode(Colors.white, BlendMode.srcIn),
                width: 23,
              )),
            ),
          ],
          title: Padding(
            padding: EdgeInsets.only(bottom: 13),
            child: Text(
              "INCOME",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: "MedioVintage"),
            ),
          ),
        ));
  }

  _showDialogAddIncome() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(
        "Tambah Pemasukan",
        style: TextStyle(color: MogeColors.primaryColor),
      ),
      content: Container(
        height: 200,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Nama Pemasukan",
              style: TextStyle(
                color: MogeColors.primaryColor,
                fontSize: 14,
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                padding: EdgeInsets.only(right: 10, top: 10, bottom: 10),
                child: TextField(
                  controller: _namaPemasukanController,
                  autofocus: false,
                  style:
                      TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                  cursorColor: MogeColors.primaryColor,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                  ),
                )),
            SizedBox(height: 15),
            Text(
              "Nominal (Rp)",
              style: TextStyle(
                color: MogeColors.primaryColor,
                fontSize: 14,
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                padding: EdgeInsets.only(right: 10, top: 10, bottom: 10),
                child: TextField(
                  controller: _nominalPemasukanController,
                  autofocus: false,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CurrencyInputFormatter()
                  ],
                  style:
                      TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                  cursorColor: MogeColors.primaryColor,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                  ),
                )),
          ],
        ),
      ),
      actions: [
        ElevatedButton(
            child: Text("BATAL", style: TextStyle(color: MogeColors.blueColor)),
            onPressed: () {
              _nominalPemasukanController.clear();
              _namaPemasukanController.clear();
              Navigator.pop(context);
            }),
        ElevatedButton(
            child:
                Text("SIMPAN", style: TextStyle(color: MogeColors.blueColor)),
            onPressed: () => _addIncome()),
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _addIncome() async {
    if (_namaPemasukanController.text.isNotEmpty &&
        _nominalPemasukanController.text.isNotEmpty) {
      HistoryModel dataIncome = HistoryModel(
          nama: _namaPemasukanController.text,
          nominal: _nominalPemasukanController.text
              .replaceAll(new RegExp(r"[^0-9]"), ""),
          date: DateFormat("yyyy-MM-dd HH:mm").format(DateTime.now()),
          type: "0",
          jenisRiwayat: "");

      
      await IncomeService.addIncome(dataIncome);
      getDataIncome();

      _nominalPemasukanController.clear();
      _namaPemasukanController.clear();
      Navigator.pop(context);
    } else {
      showFlutterToast("Lengkapi data terlebih dahulu");
    }
  }
}
