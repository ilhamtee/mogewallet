import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/model/rules_model.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/rules_service.dart';

class AddRulesPage extends StatefulWidget {
  @override
  _AddRulesPageState createState() => _AddRulesPageState();
}

class _AddRulesPageState extends State<AddRulesPage> {
  String _resultDate = "";
  DateTime? _selectedDate;

  TextEditingController _nominalController = TextEditingController();
  FocusNode _nominalFocusNode = FocusNode();

  TextEditingController _requirementController = TextEditingController();
  FocusNode _requirementFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _widgetAppBar(),
            Expanded(
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      _widgetDropdownRequirement(),
                      _widgetNominal(),
                      _resultDate != "" ? _widgetResultDate() : _logoCalendar()
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: Icon(
          Icons.add,
          color: Colors.black,
          size: 40,
        ),
        onPressed: () async {
          _addRules();
        },
      ),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                child: Icon(
                  Icons.keyboard_arrow_left,
                  size: 40,
                  color: Colors.white,
                ),
                onTap: () => Navigator.pop(context),
              ),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Rules",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetDropdownRequirement() {
    return Padding(
      padding: EdgeInsets.only(top: 20, bottom: 15, left: 50, right: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Keperluan",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.w500,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding: EdgeInsets.only(right: 10, top: 10, bottom: 10),
              child: TextField(
                controller: _requirementController,
                focusNode: _requirementFocusNode,
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: MogeColors.primaryColor)),
                ),
              ))
        ],
      ),
    );
  }

  _widgetNominal() {
    return Padding(
      padding: EdgeInsets.only(top: 20, bottom: 15, left: 50, right: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Jumlah (Rp)",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.w500,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding: EdgeInsets.only(right: 10, top: 10, bottom: 10),
              child: TextField(
                controller: _nominalController,
                focusNode: _nominalFocusNode,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: MogeColors.primaryColor,
                    ),
                  ),
                ),
              ))
        ],
      ),
    );
  }

  _widgetResultDate() {
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Tanggal Pencairan",
              style: TextStyle(
                  color: MogeColors.primaryColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 17),
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  DateTimeHelper.dateFormatSeparator(_resultDate),
                  style:
                      TextStyle(color: MogeColors.primaryColor, fontSize: 15),
                ),
                InkWell(
                  onTap: () => _pickDateDialog(),
                  child: Icon(
                    Icons.calendar_today,
                    color: MogeColors.primaryColor,
                    size: 30,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  _logoCalendar() {
    return Padding(
      padding: EdgeInsets.only(top: 10, left: 50, right: 50),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Tanggal Pencairan",
              style: TextStyle(
                  color: MogeColors.primaryColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 17),
            ),
            SizedBox(height: 10),
            Center(
                child: InkWell(
                    splashColor: Colors.transparent,
                    onTap: () {
                      _nominalFocusNode.unfocus();
                      _pickDateDialog();
                    },
                    child: SvgPicture.asset(
                      "assets/icons/calendar.svg",
                      width: 200,
                    ))),
          ],
        ),
      ),
    );
  }

  void _pickDateDialog() {
    showDatePicker(
      context: context,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: MogeColors.primaryColor,
            colorScheme: ColorScheme.light(primary: MogeColors.primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child!,
        );
      },
      initialDate: _selectedDate ?? DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime.now().add(Duration(days: 180)),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
        _resultDate = DateFormat("yyyy-MM-dd").format(pickedDate).toString();
      });
    });
  }

  _addRules() async {


    if (_requirementController.text.isNotEmpty &&
        _nominalController.text.isNotEmpty &&
        _resultDate != "") {
      RulesModel dataModel = RulesModel(
          rulesName: _requirementController.text,
          date: _resultDate,
          nominal:
              _nominalController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          isPaid: "0");

      await RulesService.addRules(dataModel);

      LoadingDialogWidget.showLoading(context,
          message: "Menambahkan rules ...");

      await Future.delayed(Duration(seconds: 2), () {
        _clearData();

        Navigator.pop(context);
        Navigator.pop(context, true);
      });
    }
  }

  _clearData() {
    _requirementController.clear();
    _resultDate = "";
    _nominalController.clear();
  }
}
