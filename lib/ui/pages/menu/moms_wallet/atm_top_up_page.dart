import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/success_top_up_page.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';
import 'package:moge_wallet/utils/services/saldo_moms_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ATMTopUpPage extends StatefulWidget {
  final String? typeTopUp;
  final String? codeReferral;
  final SaldoMomsWalletModel saldoMomsWalletModel;

  ATMTopUpPage({
    this.typeTopUp,
    this.codeReferral,
    required this.saldoMomsWalletModel,
  });

  @override
  _ATMTopUpPageState createState() => _ATMTopUpPageState();
}

class _ATMTopUpPageState extends State<ATMTopUpPage> {
  TextEditingController _nominalController = TextEditingController();
  TextEditingController _codeReferralController = TextEditingController();
  TextEditingController _noRekeningController = TextEditingController();

  FocusNode _focusNominal = FocusNode();
  FocusNode _focusCodeReferral = FocusNode();
  FocusNode _focusNoRekening = FocusNode();

  List<String> _listBank = [
    "Bank Indonesia",
    "Bank Jatim",
    "BNI",
    "BNI Syariah",
    "BRI",
    "BRI Syariah",
    "BTN",
    "BCA",
    "BCA Syariah",
    "CIMB NIAGA",
    "Mandiri"
  ];

  String _selectedBank = "BCA";
  String? _codeReferral = "";

  _getCodeReferral() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    setState(() {
      _codeReferral = sharedPref.getString(ConstantHelper.CODE_REFERRAL);
    });
  }

  @override
  void initState() {
    _getCodeReferral();
    _codeReferralController.text = widget.codeReferral ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
            bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          children: <Widget>[
            _widgetAppBar(),
            Expanded(
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      _widgetBody(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: _buttonTopUp(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Top Up ${widget.typeTopUp}",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetBody() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.typeTopUp != null
              ? widget.typeTopUp!.contains("ATM")
                  ? Center(
                      child: SvgPicture.asset(
                      "assets/icons/atm.svg",
                      width: 80,
                      colorFilter: ColorFilter.mode(
                        MogeColors.primaryColor,
                        BlendMode.srcIn,
                      ),
                    ))
                  : widget.typeTopUp?.contains("Debit") == true
                      ? Center(
                          child: SvgPicture.asset(
                          "assets/icons/card.svg",
                          width: 80,
                          colorFilter: ColorFilter.mode(
                            MogeColors.primaryColor,
                            BlendMode.srcIn,
                          ),
                        ))
                      : Center(
                          child: SvgPicture.asset(
                            "assets/icons/mobile_banking.svg",
                            width: 80,
                            colorFilter: ColorFilter.mode(
                              MogeColors.primaryColor,
                              BlendMode.srcIn,
                            ),
                          ),
                        )
              : SizedBox.shrink(),
          SizedBox(height: 30),
          Text(
            "Pilih Bank",
            style: TextStyle(color: MogeColors.primaryColor),
          ),
          SizedBox(height: 3),
          Container(
            width: MediaQuery.of(context).size.width,
            child: DropdownButtonHideUnderline(
                child: DropdownButton(
              value: _selectedBank,
              isDense: true,
              isExpanded: true,
              icon: Icon(
                Icons.arrow_drop_down,
                color: MogeColors.primaryColor,
                size: 30,
              ),
              hint: Text(
                'Pilih Bank',
                style: TextStyle(color: MogeColors.primaryColor),
              ),
              onChanged: (String? newValue) {
                if (newValue != null) {
                  setState(() {
                    _selectedBank = newValue;
                  });
                }
              },
              items: _listBank.map((String value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                );
              }).toList(),
            )),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Nomer Rekening",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _noRekeningController,
                focusNode: _focusNoRekening,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
          SizedBox(
            height: 5,
          ),
          Text(
            "Nominal Top Up (Rp)",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _nominalController,
                focusNode: _focusNominal,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
          SizedBox(height: 5),
          Text(
            "Code Referral Anak",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _codeReferralController,
                focusNode: _focusCodeReferral,
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                textCapitalization: TextCapitalization.characters,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
        ],
      ),
    );
  }

  _buttonTopUp() {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: InkWell(
        onTap: () {
          _doTopUp();
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
          height: 45,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: MogeColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Text(
            "Top Up",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
    );
  }

  _doTopUp() async {
    _focusNoRekening.unfocus();
    _focusNominal.unfocus();
    _focusCodeReferral.unfocus();

    if (_codeReferralController.text.toLowerCase() !=
        _codeReferral?.toLowerCase()) {
      showFlutterToast("Code Referral Tidak Ditemukan");
    } else if (_selectedBank.isNotEmpty &&
        _nominalController.text.isNotEmpty &&
        _noRekeningController.text.isNotEmpty) {
      


      SaldoMomsWalletModel dataSaldo = widget.saldoMomsWalletModel;
      dataSaldo.saldo = (int.parse(dataSaldo.saldo) +
              int.parse(_nominalController.text
                  .replaceAll(new RegExp(r"[^0-9]"), "")))
          .toString();

      HistoryModel dataIncome = HistoryModel(
          nama: "TopUp Moms Wallet by " + _selectedBank,
          nominal:
              _nominalController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          date: DateTimeHelper.getDateFromCurrentDate(),
          type: "3",
          jenisRiwayat: "");

      await MomsBalanceService.updateBalanceById(dataSaldo.id, dataSaldo);
      await IncomeService.addIncome(dataIncome);

      LoadingDialogWidget.showLoading(context, message: "Sedang Top Up ...");
      Future.delayed(Duration(seconds: 2), () async {
        _focusNoRekening.unfocus();
        _focusCodeReferral.unfocus();
        _focusNominal.unfocus();

        await Navigator.pushReplacement(
            context,
            new MaterialPageRoute(
              builder: (context) => SuccessTopUpPage(),
            ));

        _focusNoRekening.unfocus();
        _focusCodeReferral.unfocus();
        _focusNominal.unfocus();

        Future.delayed(Duration(seconds: 1), () async {
          Navigator.pop(context);
        });

        SystemChannels.textInput.invokeMethod('TextInput.hide');
      });
    } else {
      showFlutterToast("Lengkapi Form Top Up");
    }
  }
}
