import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:moge_wallet/models/chat_model.dart';
import 'package:moge_wallet/ui/widgets/bottom_sheet_alert.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/services/chat_service.dart';

class ChatPage extends StatefulWidget {
  final String isParent;
  final String idUser;

  ChatPage({required this.isParent, required this.idUser});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<ChatModel> _listChat = [];

  TextEditingController _chatController = TextEditingController();
  FocusNode _focusChat = FocusNode();

  _getAllDataChat() async {
    var chatFromDB = await ChatService.getAllChats();

    setState(() {
      _listChat = chatFromDB;
    });
  }

  @override
  void initState() {
    _getAllDataChat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.primaryColor,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            _widgetAppBar(),
            _listChat.length > 0
                ? _widgetListChat()
                : Expanded(
                    child: Center(
                      child: Text(
                        "Belum ada riwayat pesan",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
          ],
        ),
      ),
      bottomNavigationBar: _widgetBottomChat(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.keyboard_arrow_left,
                    size: 40, color: Colors.white),
              ),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                widget.isParent == "1" ? "Anak" : "Ibu",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            ),
            _listChat.length > 0
                ? Align(
                    child: InkWell(
                      onTap: () {
                        BottomSheetAlert(
                            title: "Hapus pesan",
                            message:
                                "Seluruh pesan akan dihapus dari riwayat chat ini",
                            primaryButtonText: "Hapus",
                            secondaryButtonText: "Batal",
                            onPrimaryButtonClick: () {
                              Navigator.pop(context);
                              _deleteAllChat();
                            }).show(context);
                      },
                      child:
                          Icon(Icons.more_vert, size: 40, color: Colors.white),
                    ),
                    alignment: Alignment.centerRight,
                  )
                : Container(),
          ],
        ));
  }

  _widgetListChat() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(
            left: 10,
            right: 10,
            top: 15,
            bottom: MediaQuery.of(context).padding.bottom),
        child: ListView.builder(
            itemCount: _listChat.length,
            padding: EdgeInsets.only(top: 0),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              ChatModel data = _listChat[index];

              if (data.idUser == widget.idUser) {
                return Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerRight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            margin:
                                EdgeInsets.only(top: 15, left: 40, right: 45),
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(
                                color: MogeColors.disableTextGreyColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Text(
                              data.chatText,
                              style: TextStyle(
                                  color: MogeColors.primaryColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 5, right: 47),
                              child: Text(
                                DateTimeHelper.getTimeofDate(data.date),
                                style: TextStyle(
                                    color: MogeColors.lightBlueColor,
                                    fontSize: 10),
                              ))
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Container(
                        width: 40,
                        height: 40,
                        padding: EdgeInsets.all(7),
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: data.isParent == "1"
                            ? SvgPicture.asset("assets/icons/mother_drawer.svg",
                                width: 20)
                            : SvgPicture.asset("assets/icons/boy.svg",
                                width: 20),
                      ),
                    ),
                  ],
                );
              } else {
                return Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin:
                                EdgeInsets.only(top: 15, left: 45, right: 45),
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(
                                color: MogeColors.disableTextGreyColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Text(
                              data.chatText,
                              style: TextStyle(
                                  color: MogeColors.primaryColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 5, left: 47),
                              child: Text(
                                DateTimeHelper.getTimeofDate(data.date),
                                style: TextStyle(
                                    color: MogeColors.lightBlueColor,
                                    fontSize: 10),
                              ))
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                        width: 40,
                        height: 40,
                        padding: EdgeInsets.all(7),
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: data.isParent == "1"
                            ? SvgPicture.asset("assets/icons/mother_drawer.svg",
                                width: 20)
                            : SvgPicture.asset("assets/icons/boy.svg",
                                width: 20),
                      ),
                    ),
                  ],
                );
              }
            }),
      ),
    );
  }

  _widgetTexFieldChat() {
    return Expanded(
        child: Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: TextField(
        controller: _chatController,
        focusNode: _focusChat,
        style: TextStyle(color: MogeColors.primaryColor, fontSize: 15),
        cursorColor: MogeColors.primaryColor,
        minLines: 1,
        maxLines: 5,
        decoration: InputDecoration(
          hintText: "Ketik Pesan ...",
          hintStyle:
              TextStyle(color: MogeColors.disableTextGreyColor, fontSize: 15),
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
        ),
      ),
    ));
  }

  _widgetButtonSendChat() {
    return GestureDetector(
      onTap: () {
        if (_chatController.text.isNotEmpty) {
          _sendChat();
          _getAllDataChat();
        } else {
          showFlutterToast("Ketik pesan terlebih dahulu");
        }
      },
      child: Container(
        padding: EdgeInsets.all(10),
        width: 45,
        height: 45,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
        child: SvgPicture.asset("assets/icons/send.svg",
            colorFilter: ColorFilter.mode(
              MogeColors.primaryColor,
              BlendMode.srcIn,
            ),
            width: 40),
      ),
    );
  }

  _widgetBottomChat() {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            _widgetTexFieldChat(),
            SizedBox(width: 5),
            _widgetButtonSendChat()
          ],
        ),
      ),
    );
  }

  _sendChat() async {
    ChatModel chatModel = ChatModel(
        id: 0,
        idUser: widget.idUser,
        chatText: _chatController.text,
        isParent: widget.isParent,
        date: DateFormat("yyyy-MM-dd HH:mm:ss")
            .format(DateTime.now())
            .toString());
    _focusChat.unfocus();
    await ChatService.addChat(chatModel);
    _chatController.clear();
  }

  _deleteAllChat() async {
    await ChatService.deleteAllChat();
    LoadingDialogWidget.showLoading(context,
        message: "Menghapus semua pesan ...");
    await Future.delayed(Duration(seconds: 2), () {
      _getAllDataChat();
      Navigator.pop(context);
    });
  }
}
