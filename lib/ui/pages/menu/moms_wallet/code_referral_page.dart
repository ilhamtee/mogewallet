import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/atm_top_up_page.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CodeReferralPage extends StatefulWidget {
  final String isParent;
  final SaldoMomsWalletModel saldoMomsWalletModel;

  CodeReferralPage({
    required this.isParent,
    required this.saldoMomsWalletModel,
  });

  @override
  _CodeReferralPageState createState() => _CodeReferralPageState();
}

class _CodeReferralPageState extends State<CodeReferralPage> {
  String? _codeReferral = "";

  TextEditingController _codeController = TextEditingController();
  FocusNode _focusCode = FocusNode();

  _getCodeReferral() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    setState(() {
      _codeReferral = sharedPref.getString(ConstantHelper.CODE_REFERRAL);
    });
  }

  @override
  void initState() {
    _getCodeReferral();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.primaryColor,
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _widgetAppBar(context),
            Expanded(
              child: Container(
                child: Center(
                  child: SingleChildScrollView(
                    child: widget.isParent == "1"
                        ? _parentCodeReferral(context)
                        : _childCodeReferral(context),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar:
          widget.isParent == "1" ? _buttonContinue(context) : SizedBox(),
    );
  }

  _widgetAppBar(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                onTap: () => Navigator.pop(context),
                child: Icon(Icons.keyboard_arrow_left,
                    size: 40, color: Colors.white),
              ),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Kode Referral",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _childCodeReferral(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: MogeColors.lightGreyColor,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.account_circle,
            size: 100,
            color: MogeColors.darkGreyColor,
          ),
          SizedBox(height: 20),
          Text(
            "Kode Referral Kamu",
            style: TextStyle(
                color: MogeColors.primaryColor,
                fontFamily: "MedioVintage",
                fontSize: 16),
          ),
          SizedBox(height: 30),
          Text(
            _codeReferral ?? 'Empty Referral Code',
            style: TextStyle(
                color: Colors.black, fontFamily: "MedioVintage", fontSize: 30),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Material(
              elevation: 5,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: InkWell(
                onTap: () {
                  Clipboard.setData(ClipboardData(text: _codeReferral ?? ''));
                  showFlutterToast("Copied");
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: MogeColors.disableTextGreyColor,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/icons/copy.svg",
                        width: 25,
                        colorFilter: ColorFilter.mode(
                          MogeColors.primaryColor,
                          BlendMode.srcIn,
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        "Copy",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.w500,
                            fontSize: 17),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _parentCodeReferral(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: MogeColors.lightGreyColor,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.account_circle,
            size: 100,
            color: MogeColors.darkGreyColor,
          ),
          SizedBox(height: 20),
          Text(
            "Masukkan Kode Referral",
            style: TextStyle(
                color: MogeColors.primaryColor,
                fontFamily: "MedioVintage",
                fontSize: 16),
          ),
          SizedBox(height: 30),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      controller: _codeController,
                      focusNode: _focusCode,
                      maxLines: 4,
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.w500),
                      decoration: InputDecoration.collapsed(
                        hintText: 'Masukkan kode referral',
                      ),
                    ),
                  ))),
        ],
      ),
    );
  }

  _buttonContinue(context) {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: GestureDetector(
        onTap: () {
          _focusCode.unfocus();
          if (_codeController.text.isNotEmpty) {
            NavigateHelpers.push(
                context,
                ATMTopUpPage(
                    codeReferral: _codeController.text,
                    saldoMomsWalletModel: widget.saldoMomsWalletModel));
            _codeController.clear();
          } else {
            showFlutterToast("Masukkan Code Referral");
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 80, vertical: 10),
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Text(
            "Lanjutkan",
            style: TextStyle(
                color: MogeColors.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 20),
          ),
        ),
      ),
    );
  }
}
