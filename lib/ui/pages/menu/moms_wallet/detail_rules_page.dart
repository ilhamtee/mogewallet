import 'package:flutter/material.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/model/rules_model.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';
import 'package:moge_wallet/utils/services/rules_service.dart';
import 'package:moge_wallet/utils/services/saldo_moms_service.dart';

class DetailRulesPage extends StatefulWidget {
  final RulesModel rulesModel;
  final SaldoMomsWalletModel saldoMomsWalletModel;

  DetailRulesPage({
    required this.rulesModel,
    required this.saldoMomsWalletModel,
  });

  @override
  _DetailRulesPageState createState() => _DetailRulesPageState();
}

class _DetailRulesPageState extends State<DetailRulesPage> {
  List<SaldoMomsWalletModel> _listSaldo = [];
  late String _totalSaldo;

  _countTotalSaldo() {
    int total = 0;
    if (_listSaldo.length > 0) {
      _listSaldo.forEach((data) {
        total += int.parse(data.saldo);
      });
    }

    setState(() {
      _totalSaldo = total.toString();
    });
  }

  _getDataSaldo() async {
    final getBalancesData = await MomsBalanceService.getAllBalances();
    setState(() {
      _listSaldo = getBalancesData;
    });
    _countTotalSaldo();
  }

  @override
  void initState() {
    _getDataSaldo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
            bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          children: <Widget>[
            _widgetAppBar(),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Kebutuhan",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                        Text(
                          widget.rulesModel.rulesName,
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Tanggal",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                        Text(
                          DateTimeHelper.dateFormatSeparator(
                              widget.rulesModel.date),
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Nominal Pembayaran",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                        Text(
                          "Rp " +
                              FormatNumber.formatterLocal(
                                  widget.rulesModel.nominal),
                          style: TextStyle(
                              color: MogeColors.primaryColor,
                              fontSize: 17,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Status",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                        Text(
                          widget.rulesModel.isPaid == "1"
                              ? "Lunas"
                              : "Belum Lunas",
                          style: TextStyle(
                              color: widget.rulesModel.isPaid == "1"
                                  ? Colors.green
                                  : Colors.red),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar:
          widget.rulesModel.isPaid == "0" ? _buttonPayment() : SizedBox(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                width: 250,
                alignment: Alignment.center,
                child: Text(
                  "Kebutuhan ${widget.rulesModel.rulesName}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontFamily: "MedioVintage"),
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                ),
              ),
            ),
            Align(
              child: InkWell(
                  onTap: () {
                    _showDialogDelete();
                  },
                  child: Icon(
                    Icons.delete,
                    size: 25,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerRight,
            ),
          ],
        ));
  }

  _buttonPayment() {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: InkWell(
        onTap: () {
          if (int.parse(_totalSaldo) > int.parse(widget.rulesModel.nominal)) {
            _showDialogPayment();
          } else {
            showFlutterToast("Saldo Moms Wallet Tidak Mencukupi!");
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 90, vertical: 10),
          height: 45,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: MogeColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Text(
            "Bayar Sekarang",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
    );
  }

  _showDialogPayment() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Pembayaran ${widget.rulesModel.rulesName}"),
      content: Text(
          "Anda yakin untuk melakukan pembayaran ${widget.rulesModel.rulesName}?"),
      actions: [
        ElevatedButton(
            child: Text(
              "TIDAK",
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () => Navigator.of(context).pop()),
        ElevatedButton(
          child: Text("YA", style: TextStyle(color: MogeColors.blueColor)),
          onPressed: () => _doPayment(),
        ),
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _showDialogDelete() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Hapus Kebutuhan ${widget.rulesModel.rulesName}"),
      content: Text(
          "Anda yakin untuk menghapus kebutuhan ${widget.rulesModel.rulesName}?"),
      actions: [
        ElevatedButton(
            child: Text(
              "TIDAK",
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () => Navigator.of(context).pop()),
        ElevatedButton(
            child: Text("YA", style: TextStyle(color: MogeColors.blueColor)),
            onPressed: () => _doDelete()),
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void _doPayment() async {
    Navigator.pop(context);

    RulesModel dataRules = widget.rulesModel;
    SaldoMomsWalletModel dataSaldo = widget.saldoMomsWalletModel;
    dataSaldo.saldo =
        (int.parse(dataSaldo.saldo) - int.parse(dataRules.nominal)).toString();

    HistoryModel dataIncome = HistoryModel(
        nama: "Kebutuhan ${widget.rulesModel.rulesName}",
        nominal:
            widget.rulesModel.nominal.replaceAll(new RegExp(r"[^0-9]"), ""),
        date: DateTimeHelper.getDateFromCurrentDate(),
        type: "1",
        jenisRiwayat: "");

    dataRules.isPaid = "1";

    await IncomeService.addIncome(dataIncome);
    await RulesService.updateRules(dataRules.id!, dataRules);
    await MomsBalanceService.updateBalanceById(dataSaldo.id, dataSaldo);

    LoadingDialogWidget.showLoading(context, message: "Proses Pembayaran ...");
    Future.delayed(Duration(seconds: 2), () async {
      Navigator.pop(context);
      Navigator.pop(context, true);
    });
  }

  _doDelete() async {
    await RulesService.deleteRules(widget.rulesModel.id!);
    Navigator.pop(context);
    Navigator.pop(context);
  }
}
