import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/income_service.dart';

class HistoryTopUpPage extends StatefulWidget {
  @override
  _HistoryTopUpPageState createState() => _HistoryTopUpPageState();
}

class _HistoryTopUpPageState extends State<HistoryTopUpPage> {
  List<HistoryModel> _listIncome = [];

  getDataIncome() async {
    
    var getDataIncome = await IncomeService.getDataIncome();
    setState(() {
      _listIncome =
          getDataIncome.where((element) => element.type == "3").toList();
    });
  }

  @override
  void initState() {
    getDataIncome();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      appBar: _widgetAppBar(),
      body: Container(
        color: MogeColors.lightBlueColor,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            _listIncome.length > 0
                ? Expanded(
                    child: Container(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listIncome.length,
                          itemBuilder: (context, index) {
                            HistoryModel data = _listIncome[index];
                            return _widgetItemIncome(data);
                          }),
                    ),
                  )
                : Expanded(
                    child: Center(
                    child: Text(
                      "Belum Ada Riwayat Top Up",
                      style: TextStyle(color: MogeColors.primaryColor),
                    ),
                  ))
          ],
        ),
      ),
    );
  }

  _widgetItemIncome(HistoryModel data) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          border:
              Border.all(width: 2.0, color: Color.fromRGBO(206, 221, 242, 1)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data.nama,
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 15),
                  ),
                  SizedBox(height: 5),
                  Text(
                    DateTimeHelper.dateParseLocalSeparator(data.date),
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 12),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.green),
                      ),
                      SizedBox(width: 5),
                      Text(
                        "Pemasukan ",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontWeight: FontWeight.w500,
                            fontSize: 15),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Text(
                    FormatNumber.formatter(data.nominal),
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 15),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _widgetAppBar() {
    return PreferredSize(
        preferredSize: Size.fromHeight(50.0), // here the desired height
        child: AppBar(
          backgroundColor: MogeColors.primaryColor,
          centerTitle: true,
          elevation: 0,
          leading: Padding(
            padding: EdgeInsets.only(left: 25),
            child: InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.keyboard_arrow_left,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
          title: Padding(
            padding: EdgeInsets.only(bottom: 0),
            child: Text(
              "Riwayat Top Up",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: "MedioVintage"),
            ),
          ),
        ));
  }
}
