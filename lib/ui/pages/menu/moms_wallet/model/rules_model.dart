class RulesModel {
  int? id;
  String rulesName;
  String date;
  String nominal;
  String isPaid;

  RulesModel({
    this.id,
    required this.rulesName,
    required this.date,
    required this.nominal,
    required this.isPaid,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "rulesName": rulesName,
      "date": date,
      "nominal": nominal,
      "isPaid": isPaid,
    };
  }
}
