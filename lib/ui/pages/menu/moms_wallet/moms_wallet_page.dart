import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/models/user_model.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/add_rules_page.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/chat_page.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/code_referral_page.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/detail_rules_page.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/history_top_up.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/model/rules_model.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/top_up_page.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/rules_service.dart';
import 'package:moge_wallet/utils/services/saldo_moms_service.dart';
import 'package:moge_wallet/utils/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MomsWalletPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  MomsWalletPage({required this.scaffoldState});

  @override
  _MomsWalletPageState createState() => _MomsWalletPageState();
}

class _MomsWalletPageState extends State<MomsWalletPage> {
  List<RulesModel> _dataRules = [];
  List<UserModel> _currentUser = [];
  bool _isParent = false;
  String? _idUser;

  List<SaldoMomsWalletModel> _listSaldo = [];
  String? _totalSaldo;

  _countTotalSaldo() {
    int total = 0;
    if (_listSaldo.length > 0) {
      _listSaldo.forEach((data) {
        total += int.parse(data.saldo);
      });
    }

    setState(() {
      _totalSaldo = total.toString();
    });
  }

  _getDataSaldo() async {
    final getBalancesData = await MomsBalanceService.getAllBalances();
    setState(() {
      _listSaldo = getBalancesData;
    });
    _countTotalSaldo();
  }

  _getDataRules() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    String? idUser = sharedPref.getString(ConstantHelper.ID_USER);

    List<UserModel> listAllUser = await UserService.getDataUser();
    _currentUser =
        listAllUser.where((user) => user.id.toString() == idUser).toList();

    List<RulesModel> _rulesFromDB = await RulesService.getDataRules();

    if (_currentUser.first.isParent == "1") {
      setState(() {
        _isParent = true;
      });
    }

    setState(() {
      _dataRules = _rulesFromDB;
      _idUser = sharedPref.getString(ConstantHelper.ID_USER);
    });
  }

  @override
  void initState() {
    _getDataRules();
    _getDataSaldo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            _widgetBackgroundHeader(),
            _widgetBodyMomsWallet(),
            _widgetHeaderMomsWallet(),
            _widgetAppBar(),
          ],
        ));
  }

  _widgetBackgroundHeader() {
    return Positioned(
      top: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 300,
        color: MogeColors.primaryColor,
        alignment: Alignment.center,
        padding: EdgeInsets.only(bottom: 150),
        child: Text(
          "Rp ${FormatNumber.formatterLocal(_totalSaldo ?? '0')}",
          style: TextStyle(
              color: Colors.white, fontSize: 25, fontFamily: "MedioVintage"),
        ),
      ),
    );
  }

  _widgetHeaderMomsWallet() {
    return Positioned(
      top: 140,
      left: 20,
      right: 20,
      child: Container(
        height: 120,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Row(
          children: <Widget>[
            //Rules
            _isParent
                ? Container()
                : Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () async {
                        bool? result = await Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (context) => AddRulesPage(),
                            ));

                        if (result != null || result == true) {
                          _getDataRules();
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              "assets/icons/rules.svg",
                              colorFilter: ColorFilter.mode(
                                MogeColors.primaryColor,
                                BlendMode.srcIn,
                              ),
                              width: 40,
                            ),
                            SizedBox(height: 5),
                            Text(
                              "Rules",
                              style: TextStyle(color: MogeColors.primaryColor),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),

            _isParent
                ? Container()
                : Container(
                    width: 1,
                    height: 80,
                    color: MogeColors.primaryColor,
                  ),

            //Chat
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: () => NavigateHelpers.push(
                  context,
                  ChatPage(
                    isParent: _isParent ? "1" : "0",
                    idUser: _idUser ?? '',
                  ),
                ),
                child: Container(
                  alignment: Alignment.center,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset(
                        "assets/icons/chat.svg",
                        colorFilter: ColorFilter.mode(
                          MogeColors.primaryColor,
                          BlendMode.srcIn,
                        ),
                        width: 40,
                      ),
                      SizedBox(height: 5),
                      Text(
                        "Chat",
                        style: TextStyle(color: MogeColors.primaryColor),
                      )
                    ],
                  ),
                ),
              ),
            ),

            _isParent
                ? Container(
                    width: 1, height: 80, color: MogeColors.primaryColor)
                : Container(),

            //Top Up
            _isParent
                ? Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () async {
                        await Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (context) => TopUpPage(
                                  saldoMomsWalletModel: _listSaldo.first),
                            ));
                        _getDataSaldo();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              "assets/icons/top_up.svg",
                              colorFilter: ColorFilter.mode(
                                _isParent
                                    ? MogeColors.primaryColor
                                    : MogeColors.disableTextGreyColor,
                                BlendMode.srcIn,
                              ),
                              width: 40,
                            ),
                            SizedBox(height: 5),
                            Text(
                              "Top Up",
                              style: TextStyle(
                                  color: _isParent
                                      ? MogeColors.primaryColor
                                      : MogeColors.disableTextGreyColor),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : Container(),

            _isParent
                ? Container(
                    width: 1,
                    height: 80,
                    color: MogeColors.primaryColor,
                  )
                : Container(),

            //Riwayat Top Up
            _isParent
                ? Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () {
                        NavigateHelpers.push(context, HistoryTopUpPage());
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              "assets/icons/riwayat_top_up.svg",
                              colorFilter: ColorFilter.mode(
                                _isParent
                                    ? MogeColors.primaryColor
                                    : MogeColors.disableTextGreyColor,
                                BlendMode.srcIn,
                              ),
                              width: 40,
                            ),
                            SizedBox(height: 5),
                            Padding(
                              padding: EdgeInsets.only(left: 8),
                              child: Text(
                                "Riwayat Top Up",
                                style: TextStyle(
                                  color: _isParent
                                      ? MogeColors.primaryColor
                                      : MogeColors.disableTextGreyColor,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  _widgetAppBar() {
    return Positioned(
      top: 0,
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 60,
          alignment: Alignment.bottomLeft,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Stack(
            children: <Widget>[
              Align(
                child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer(),
                ),
                alignment: Alignment.centerLeft,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "MOM'S WALLET",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: "MedioVintage"),
                ),
              ),
              Align(
                child: InkWell(
                  onTap: () async {
                    await Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => CodeReferralPage(
                              isParent: _isParent ? "1" : "0",
                              saldoMomsWalletModel: _listSaldo.first),
                        ));
                    _getDataSaldo();
                  },
                  child: Container(
                    width: 40,
                    decoration: BoxDecoration(
                        color: MogeColors.primaryDarkColor,
                        shape: BoxShape.circle),
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(7),
                    child: SvgPicture.asset(
                      "assets/icons/notif_moms.svg",
                      colorFilter: ColorFilter.mode(
                        Colors.white,
                        BlendMode.srcIn,
                      ),
                    ),
                  ),
                ),
                alignment: Alignment.centerRight,
              ),
            ],
          )),
    );
  }

  _widgetBodyMomsWallet() {
    return Positioned(
      bottom: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.70,
        padding: EdgeInsets.only(
          top: 90,
          left: 10,
          right: 10,
        ),
        decoration: BoxDecoration(
            color: MogeColors.lightBlueColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(80), topLeft: Radius.circular(80))),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                color: MogeColors.darkBlueColor,
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: _dataRules.length > 0
                    ? _widgetListRules()
                    : Center(
                        child: Text(
                          "Anda belum membuat rules apapun",
                          style: TextStyle(color: MogeColors.primaryColor),
                        ),
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _widgetListRules() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "KEBUTUHAN",
          style: TextStyle(
              color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        ),
        Expanded(
          child: Container(
            child: ListView.separated(
                shrinkWrap: true,
                padding: EdgeInsets.symmetric(horizontal: 10),
                itemBuilder: (context, index) {
                  RulesModel data = _dataRules[index];
                  return InkWell(
                    onTap: () async {
                      bool result = await Navigator.push(
                          context,
                          new MaterialPageRoute(
                            builder: (context) => DetailRulesPage(
                              rulesModel: data,
                              saldoMomsWalletModel: _listSaldo.first,
                            ),
                          ));

                      if (result == true) {
                        _getDataRules();
                        _getDataSaldo();
                        showFlutterToast("Pembayaran Berhasil");
                      } else {
                        _getDataRules();
                        _getDataSaldo();
                      }
                    },
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: 40,
                            alignment: Alignment.centerLeft,
                            child: Text(
                              data.rulesName,
                              style: TextStyle(
                                  fontSize: 16, color: MogeColors.primaryColor),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerLeft,
                            height: 40,
                            child: Text(
                              DateTimeHelper.dateFormatSeparator(data.date),
                              style: TextStyle(
                                  fontSize: 16, color: MogeColors.primaryColor),
                            ),
                          ),
                        ),
                        Container(
                            width: 70,
                            height: 40,
                            alignment: Alignment.centerRight,
                            child: data.isPaid == "1"
                                ? Icon(
                                    Icons.check_circle,
                                    size: 30,
                                    color: Colors.green,
                                  )
                                : Icon(
                                    Icons.clear,
                                    size: 30,
                                    color: Colors.red,
                                  )),
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider(
                    color: MogeColors.primaryColor,
                  );
                },
                itemCount: _dataRules.length),
          ),
        ),
      ],
    );
  }
}
