import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/atm_top_up_page.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/saldo_moms_service.dart';

class TopUpPage extends StatefulWidget {
  final SaldoMomsWalletModel saldoMomsWalletModel;

  TopUpPage({
    required this.saldoMomsWalletModel,
  });

  @override
  _TopUpPageState createState() => _TopUpPageState();
}

class _TopUpPageState extends State<TopUpPage> {
  List<SaldoMomsWalletModel> _listSaldo = [];
  String? _totalSaldo;

  _countTotalSaldo() {
    int total = 0;
    if (_listSaldo.length > 0) {
      _listSaldo.forEach((data) {
        total += int.parse(data.saldo);
      });
    }

    setState(() {
      _totalSaldo = total.toString();
    });
  }

  _getDataSaldo() async {
    final getBalancesData = await MomsBalanceService.getAllBalances();
    setState(() {
      _listSaldo = getBalancesData;
    });
    _countTotalSaldo();
  }

  @override
  void initState() {
    _getDataSaldo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MogeColors.lightBlueColor,
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                _widgetHeader(),
                _widgetBody(),
                _widgetAppBar()
              ],
            )));
  }

  _widgetAppBar() {
    return Positioned(
      top: 0,
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 60,
          alignment: Alignment.bottomLeft,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Stack(
            children: <Widget>[
              Align(
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(Icons.keyboard_arrow_left,
                      size: 40, color: Colors.white),
                ),
                alignment: Alignment.centerLeft,
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Top Up",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: "MedioVintage"),
                ),
              )
            ],
          )),
    );
  }

  _widgetHeader() {
    return Positioned(
      top: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 300,
        color: MogeColors.primaryColor,
        child: Padding(
          padding: EdgeInsets.only(bottom: 100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 90, bottom: 10),
                child: Text(
                  "Saldo",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: "MedioVintage"),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  "Rp ${FormatNumber.formatterLocal(_totalSaldo ?? '0')}",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 25,
                      fontFamily: "MedioVintage"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _widgetBody() {
    return Positioned(
      bottom: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.70,
        decoration: BoxDecoration(
            color: MogeColors.lightBlueColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30))),
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _textMetodeTopUp(),
              SizedBox(height: 20),
              _widgetATM(),
              _widgetCard(),
              _widgetMobileBanking()
            ],
          ),
        ),
      ),
    );
  }

  _textMetodeTopUp() {
    return Column(
      children: <Widget>[
        Container(
          width: 120,
          height: 3,
          color: MogeColors.primaryColor,
        ),
        SizedBox(height: 10),
        Text(
          "Metode Top Up",
          style: TextStyle(
            color: MogeColors.primaryColor,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }

  _widgetATM() {
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => ATMTopUpPage(
                  typeTopUp: "ATM",
                  saldoMomsWalletModel: widget.saldoMomsWalletModel),
            ));
        _getDataSaldo();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 10, bottom: 10),
              child: Row(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/icons/atm.svg",
                    width: 50,
                    colorFilter: ColorFilter.mode(
                      MogeColors.primaryColor,
                      BlendMode.srcIn,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    "ATM",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  )
                ],
              ),
            ),
            Divider(
              color: MogeColors.primaryColor,
              height: 2,
            )
          ],
        ),
      ),
    );
  }

  _widgetCard() {
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => ATMTopUpPage(
                  typeTopUp: "Kartu Debit",
                  saldoMomsWalletModel: widget.saldoMomsWalletModel),
            ));
        _getDataSaldo();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 10, bottom: 10),
              child: Row(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/icons/card.svg",
                    width: 50,
                    colorFilter: ColorFilter.mode(
                      MogeColors.primaryColor,
                      BlendMode.srcIn,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Kartu Debit",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  )
                ],
              ),
            ),
            Divider(
              color: MogeColors.primaryColor,
              height: 2,
            )
          ],
        ),
      ),
    );
  }

  _widgetMobileBanking() {
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
            context,
            new MaterialPageRoute(
              builder: (context) => ATMTopUpPage(
                  typeTopUp: "Mobile Banking",
                  saldoMomsWalletModel: widget.saldoMomsWalletModel),
            ));
        _getDataSaldo();
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 10, bottom: 10),
              child: Row(
                children: <Widget>[
                  SvgPicture.asset(
                    "assets/icons/mobile_banking.svg",
                    width: 50,
                    colorFilter: ColorFilter.mode(
                      MogeColors.primaryColor,
                      BlendMode.srcIn,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Internet / \nMobile Banking",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  )
                ],
              ),
            ),
            Divider(
              color: MogeColors.primaryColor,
              height: 2,
            )
          ],
        ),
      ),
    );
  }
}
