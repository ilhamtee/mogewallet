import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/edit_dreams_page.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/top_up_dreams_page.dart';
import 'package:moge_wallet/ui/widgets/widget_bottom_sheet.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/global_method_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';

class DetailDreamsPage extends StatefulWidget {
  final DreamsModel dreamsModel;

  DetailDreamsPage({required this.dreamsModel});

  @override
  _DetailDreamsPageState createState() => _DetailDreamsPageState();
}

class _DetailDreamsPageState extends State<DetailDreamsPage> {
  late DreamsModel _dataDreams;

  @override
  void initState() {
    _dataDreams = widget.dreamsModel;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
            bottom: MediaQuery.of(context).padding.bottom),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _widgetAppBar(),
              _widgetHeader(),
              _widgetBody(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _bottomDetail(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                _dataDreams.namaImpian,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetHeader() {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 120,
                height: 120,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: !GlobalMethodHelper.isEmpty(
                                _dataDreams.imagePath)
                            ? MemoryImage(base64Decode(_dataDreams.imagePath))
                            : AssetImage("assets/no_image.png")
                                as ImageProvider,
                        fit: BoxFit.fill)),
              ),
              SizedBox(height: 10),
              Text(
                "Kamu Sudah Menabung",
                style: TextStyle(
                    color: MogeColors.primaryColor,
                    fontWeight: FontWeight.w300,
                    fontSize: 17),
              ),
              SizedBox(height: 10),
              Text(
                "Rp " + FormatNumber.formatterLocal(_dataDreams.danaTerkumpul),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 25),
              ),
              SizedBox(height: 5),
              Text(
                "Dari Rp " +
                    FormatNumber.formatterLocal(_dataDreams.danaPencapaian),
                style: TextStyle(
                    color: MogeColors.primaryColor,
                    fontWeight: FontWeight.w300,
                    fontSize: 17),
              ),
            ],
          ),
        ),
        Container(
          height: 3,
          color: MogeColors.primaryColor,
        )
      ],
    );
  }

  _widgetBody() {
    return Column(
      children: <Widget>[
        SizedBox(height: 20),
        Container(
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 15),
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              color: MogeColors.darkGreyColor,
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Tanggal Pencapaian",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                    fontSize: 18),
              ),
              Text(
                DateTimeHelper.dateParseLocalSeparator(
                    _dataDreams.tanggalPencapaian),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              )
            ],
          ),
        ),
        SizedBox(height: 10),
        Container(
          padding: EdgeInsets.symmetric(vertical: 30, horizontal: 15),
          margin: EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
              color: MogeColors.darkGreyColor,
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Tabungan Bulanan",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                    fontSize: 18),
              ),
              Text(
                "Rp " +
                    FormatNumber.formatterLocal(
                        _dataDreams.nominalTabunganOtomatis),
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 22),
              )
            ],
          ),
        )
      ],
    );
  }

  _bottomDetail() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              //Top Up
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () async {
                    if (widget.dreamsModel.isActive == "1") {
                      _showDialogTopUp();
                    } else {
                      showFlutterToast("Impian Sedang Ditunda");
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          "assets/icons/top_up_dreams.svg",
                          colorFilter: ColorFilter.mode(
                            widget.dreamsModel.isActive == "1"
                                ? MogeColors.primaryColor
                                : MogeColors.darkGreyColor,
                            BlendMode.srcIn,
                          ),
                          width: 40,
                        ),
                        SizedBox(height: 15),
                        Text(
                          "Top Up",
                          style: TextStyle(
                              color: widget.dreamsModel.isActive == "1"
                                  ? MogeColors.primaryColor
                                  : MogeColors.darkGreyColor,
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )
                      ],
                    ),
                  ),
                ),
              ),

              Container(
                width: 1,
                height: 60,
                color: MogeColors.primaryColor,
                margin: EdgeInsets.only(bottom: 15),
              ),

              //Edit
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    NavigateHelpers.push(
                      context,
                      EditDreamsPage(dreamsModel: _dataDreams),
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          "assets/icons/edit.svg",
                          colorFilter: ColorFilter.mode(
                            MogeColors.primaryColor,
                            BlendMode.srcIn,
                          ),
                          width: 30,
                        ),
                        SizedBox(height: 15),
                        Text(
                          "Edit",
                          style: TextStyle(
                              color: MogeColors.primaryColor,
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )
                      ],
                    ),
                  ),
                ),
              ),

              Container(
                width: 1,
                height: 60,
                color: MogeColors.primaryColor,
                margin: EdgeInsets.only(bottom: 15),
              ),

              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: () {
                    WidgetBottomSheet().show(context, widget.dreamsModel);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(bottom: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SvgPicture.asset(
                              "assets/icons/circle.svg",
                              colorFilter: ColorFilter.mode(
                                MogeColors.primaryColor,
                                BlendMode.srcIn,
                              ),
                              width: 10,
                            ),
                            SizedBox(width: 5),
                            SvgPicture.asset(
                              "assets/icons/circle.svg",
                              colorFilter: ColorFilter.mode(
                                MogeColors.primaryColor,
                                BlendMode.srcIn,
                              ),
                              width: 10,
                            ),
                            SizedBox(width: 5),
                            SvgPicture.asset(
                              "assets/icons/circle.svg",
                              colorFilter: ColorFilter.mode(
                                MogeColors.primaryColor,
                                BlendMode.srcIn,
                              ),
                              width: 10,
                            ),
                          ],
                        ),
                        SizedBox(height: 15),
                        Text(
                          "Lainnya",
                          style: TextStyle(
                              color: MogeColors.primaryColor,
                              fontWeight: FontWeight.w400,
                              fontSize: 15),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  _showDialogTopUp() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Pilih Metode Top Up"),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () async {
              Navigator.pop(context);
              DreamsModel result = await Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => TopUpDreamsPage(
                        typeTopUp: "ATM", dreamsModel: _dataDreams),
                  ));

              if (!GlobalMethodHelper.isEmpty(result)) {
                setState(() {
                  _dataDreams = result;
                });
              }
            },
            child: Text(
              "ATM",
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () async {
              Navigator.pop(context);
              DreamsModel result = await Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => TopUpDreamsPage(
                        typeTopUp: "Kartu Debit", dreamsModel: _dataDreams),
                  ));

              if (!GlobalMethodHelper.isEmpty(result)) {
                setState(() {
                  _dataDreams = result;
                });
                showFlutterToast("Berhasil Top Up");
              }
            },
            child: Text(
              "Kartu Debit",
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(height: 15),
          GestureDetector(
            onTap: () async {
              Navigator.pop(context);
              DreamsModel result = await Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => TopUpDreamsPage(
                        typeTopUp: "Mobile Banking", dreamsModel: _dataDreams),
                  ));

              if (!GlobalMethodHelper.isEmpty(result)) {
                setState(() {
                  _dataDreams = result;
                });
                showFlutterToast("Berhasil Top Up");
              }
            },
            child: Text(
              "Internet / Mobile Banking",
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
