import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/base64_helper.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/global_method_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/dreams_service.dart';

class EditDreamsPage extends StatefulWidget {
  final DreamsModel dreamsModel;

  EditDreamsPage({required this.dreamsModel});

  @override
  _EditDreamsPageState createState() => _EditDreamsPageState();
}

class _EditDreamsPageState extends State<EditDreamsPage> {
  bool _isLockDate = false;

  List<String> _typeTabunganOtomatis = [
    "Harian",
    "Mingguan",
    "Bulanan",
    "Tahunan"
  ];
  String _selectedDropDown = "Bulanan";

  TextEditingController _targetDanaController = TextEditingController();
  TextEditingController _danaAwalController = TextEditingController();
  TextEditingController _namaImpianController = TextEditingController();
  TextEditingController _nominalDebitController =
      TextEditingController(text: "500.000");

  FocusNode _focusTargetDana = FocusNode();
  FocusNode _focusDanaAwal = FocusNode();
  FocusNode _focusNominalDebit = FocusNode();
  FocusNode _focusNamaImpian = FocusNode();

  DateTime? _selectedDate;
  String? _resultDate;
  String? _resultImage;

  void _setDataDreams() {
    setState(() {
      _resultDate = widget.dreamsModel.tanggalPencapaian;
      _selectedDate = DateTime.parse(widget.dreamsModel.tanggalPencapaian);
      _namaImpianController.text = widget.dreamsModel.namaImpian;
      _targetDanaController.text =
          FormatNumber.formatterLocal(widget.dreamsModel.danaPencapaian);
      _danaAwalController.text =
          FormatNumber.formatterLocal(widget.dreamsModel.danaAwal);
      _nominalDebitController.text = FormatNumber.formatterLocal(
          widget.dreamsModel.nominalTabunganOtomatis);
      _isLockDate =
          widget.dreamsModel.isKunciTglPencapaian == "1" ? true : false;
      _resultImage = widget.dreamsModel.imagePath;
      _selectedDropDown = widget.dreamsModel.tabunganOtomatis;
    });
  }

  @override
  void initState() {
    _setDataDreams();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
            bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          children: <Widget>[
            _widgetAppBar(),
            Expanded(
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      _widgetHeader(),
                      _widgetBody(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: _buttonContinue(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Edit Impian",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetHeader() {
    return Column(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.30,
          color: Color.fromRGBO(163, 181, 207, 1),
          alignment: Alignment.center,
          child: Container(
            width: 160,
            decoration: BoxDecoration(
                color: Color.fromRGBO(121, 143, 173, 1),
                shape: BoxShape.circle),
            alignment: Alignment.center,
            child: InkWell(
              onTap: () {
                _pickDateDialog();
              },
              child: Container(
                width: 150,
                decoration: BoxDecoration(
                    color: MogeColors.primaryColor, shape: BoxShape.circle),
                alignment: Alignment.center,
                child: _resultDate != null
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Tercapai Pada",
                            style: TextStyle(color: Colors.white, fontSize: 12),
                          ),
                          SizedBox(height: 10),
                          Text(
                            DateTimeHelper.getDaysFromCurrentDate(_resultDate),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 25,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 5),
                          Text(
                            DateTimeHelper.getMonthFromCurrentDate(_resultDate),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w300),
                          ),
                          SizedBox(height: 5),
                          Text(
                            DateTimeHelper.getYearFromCurrentDate(_resultDate),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontWeight: FontWeight.w200),
                          ),
                        ],
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "TENTUKAN",
                            style: TextStyle(color: Colors.white, fontSize: 13),
                          ),
                          SizedBox(height: 5),
                          Text(
                            "TANGGAL",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
              ),
            ),
          ),
        ),
        Container(
          height: 45,
          color: Color.fromRGBO(121, 143, 173, 1),
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Kunci Tanggal Pencapaian",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w300,
                    fontSize: 15),
              ),
              Switch(
                value: _isLockDate,
                onChanged: (value) {
                  setState(() {
                    _isLockDate = value;
                  });
                },
                activeTrackColor: MogeColors.darkBlueColor,
                activeColor: MogeColors.primaryColor,
              ),
            ],
          ),
        ),
      ],
    );
  }

  _widgetBody() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          !GlobalMethodHelper.isEmpty(_resultImage)
              ? _widgetImage()
              : _widgetAddImage(),

          //Input Nama Impian
          Text(
            "Nama Impian",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _namaImpianController,
                focusNode: _focusNamaImpian,
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),

          //Input Dana Pencapaian
          Text(
            "Dana Pencapaian (Rp)",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _targetDanaController,
                focusNode: _focusTargetDana,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),

          SizedBox(height: 5),

          //Input Dana Awal
          Text(
            "Dana Awal (Rp)",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _danaAwalController,
                focusNode: _focusDanaAwal,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),

          SizedBox(height: 5),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: 170,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Tabungan Otomatis",
                      style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      height: 50,
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          color: MogeColors.primaryColor),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: MogeColors.lightBlueColor),
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                          value: _selectedDropDown,
                          isDense: true,
                          isExpanded: true,
                          icon: Icon(
                            Icons.arrow_drop_down,
                            color: MogeColors.primaryColor,
                            size: 30,
                          ),
                          hint: Text('Pilih Jenis Tabungan'),
                          onChanged: (String? newValue) {
                            if (newValue != null) {
                              setState(() {
                                _selectedDropDown = newValue;
                              });
                            }
                          },
                          items: _typeTabunganOtomatis.map((String value) {
                            return DropdownMenuItem(
                              value: value,
                              child: Text(
                                value,
                                style:
                                    TextStyle(color: MogeColors.primaryColor),
                              ),
                            );
                          }).toList(),
                        )),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Jumlah (Rp)",
                        style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                      SizedBox(height: 5),
                      Container(
                        height: 50,
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: MogeColors.primaryColor),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: MogeColors.lightBlueColor),
                          padding: EdgeInsets.symmetric(horizontal: 5),
                          child: TextField(
                            controller: _nominalDebitController,
                            focusNode: _focusNominalDebit,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly,
                              CurrencyInputFormatter()
                            ],
                            style: TextStyle(
                                color: MogeColors.primaryColor, fontSize: 18),
                            cursorColor: MogeColors.primaryColor,
                            decoration: InputDecoration(
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                contentPadding: EdgeInsets.only(bottom: 6)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  _buttonContinue() {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: InkWell(
        onTap: () {
          _updateDreams();
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
          height: 45,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: MogeColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Text(
            "Simpan",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
    );
  }

  _widgetAddImage() {
    return Center(
      child: InkWell(
        onTap: () => _showImagePicker(),
        child: Container(
          width: 100,
          height: 80,
          margin: EdgeInsets.only(bottom: 20),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              border: Border.all(
                  color: Color.fromRGBO(163, 181, 207, 1), width: 2)),
          child: Icon(
            Icons.photo_camera,
            color: MogeColors.primaryColor,
            size: 40,
          ),
        ),
      ),
    );
  }

  Widget _widgetImage() {
    return Center(
      child: InkWell(
        onTap: () => _showImagePicker(),
        child: Container(
          width: 140,
          height: 130,
          margin: EdgeInsets.only(bottom: 20),
          child: Image.memory(
            base64Decode(_resultImage ?? ''),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  //Method for showing the date picker
  void _pickDateDialog() {
    showDatePicker(
      context: context,
      builder: (BuildContext context, Widget? child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: MogeColors.primaryColor,
            colorScheme: ColorScheme.light(primary: MogeColors.primaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child!,
        );
      },
      initialDate: _selectedDate ?? DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime.now().add(Duration(days: 180)),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
        _resultDate = DateFormat("yyyy-MM-dd").format(pickedDate).toString();
      });
    });
  }

  void _imgFromCamera() async {
    final picker = ImagePicker();
    XFile? image =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 80);

    if (image != null) {
      setState(() {
        _resultImage =
            Base64Helper.base64String(File(image.path).readAsBytesSync());
      });
    }
  }

  void _imgFromGallery() async {
    final picker = ImagePicker();
    XFile? image =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 80);

    if (image != null) {
      setState(() {
        _resultImage =
            Base64Helper.base64String(File(image.path).readAsBytesSync());
      });
    }
  }

  void _showImagePicker() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(
                        Icons.photo_library,
                        color: MogeColors.primaryColor,
                      ),
                      title: new Text(
                        'Gallery',
                        style: TextStyle(color: MogeColors.primaryColor),
                      ),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(
                      Icons.photo_camera,
                      color: MogeColors.primaryColor,
                    ),
                    title: new Text(
                      'Camera',
                      style: TextStyle(color: MogeColors.primaryColor),
                    ),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _updateDreams() async {
    if (!GlobalMethodHelper.isEmpty(_resultDate) &&
        !GlobalMethodHelper.isEmpty(_targetDanaController.text) &&
        !GlobalMethodHelper.isEmpty(_danaAwalController.text) &&
        !GlobalMethodHelper.isEmpty(_namaImpianController.text)) {
      DreamsModel data = DreamsModel(
          imagePath: _resultImage ?? "",
          imageDummy: "",
          isKunciTglPencapaian: _isLockDate ? "1" : "0",
          tanggalPencapaian: _resultDate!,
          tabunganOtomatis: _selectedDropDown,
          nominalTabunganOtomatis: _nominalDebitController.text
              .replaceAll(new RegExp(r"[^0-9]"), ""),
          danaTerkumpul:
              _danaAwalController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          danaAwal:
              _danaAwalController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          danaPencapaian:
              _targetDanaController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          namaImpian: _namaImpianController.text,
          isActive: "1");
      _focusNamaImpian.unfocus();
      _focusNominalDebit.unfocus();
      _focusDanaAwal.unfocus();
      _focusTargetDana.unfocus();
      SystemChannels.textInput.invokeMethod('TextInput.hide');

      await DreamsService.updateDreams(widget.dreamsModel.id!, data);

      LoadingDialogWidget.showLoading(context, message: "Edit Impian ...");
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pop(context);
        Navigator.pop(context);
        Navigator.pop(context, true);
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      });
    } else {
      showFlutterToast("Lengkapi semua data");
    }
  }
}
