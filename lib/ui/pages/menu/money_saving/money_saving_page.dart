import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/add_dreams_page.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/detail_dreams_page.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/global_method_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/dreams_service.dart';

class MoneySavingPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  MoneySavingPage({required this.scaffoldState});

  @override
  _MoneySavingPageState createState() => _MoneySavingPageState();
}

class _MoneySavingPageState extends State<MoneySavingPage> {
  List<DreamsModel> _listDataDreams = [];

  Future<void> _getDataDreams() async {
    final getDreamsFromDB = await DreamsService.getDataDreams();

    setState(() {
      _listDataDreams = getDreamsFromDB;
    });
  }

  @override
  void initState() {
    _getDataDreams();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: MogeColors.lightBlueColor,
      child: Column(
        children: <Widget>[
          _widgetAppBar(),
          _widgetHeader(),
          _listDataDreams.length > 0
              ? _listWidgetDreams()
              : Expanded(
                  child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/icons/broke.svg",
                        width: 100,
                      ),
                      SizedBox(height: 10),
                      Text(
                        "Anda Belum Membuat Impian",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ))
        ],
      ),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer()),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Dream Saver",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
      decoration: BoxDecoration(color: Color.fromRGBO(130, 150, 179, 1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Rp 560.000.000",
            style: TextStyle(
                color: Colors.white, fontSize: 25, fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 10),
          Text(
            "Menabung otomatis dan \npercepat wujudkan mimpimu",
            style: TextStyle(
                color: Colors.white, fontSize: 20, fontWeight: FontWeight.w500),
          ),
          SizedBox(height: 20),
          InkWell(
            onTap: () async {
              bool? result = await Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => AddDreamsPage(),
                  ));

              if (result != null) {
                await _getDataDreams();
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [MogeColors.primaryColor, MogeColors.blueColor]),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      bottomLeft: Radius.circular(40),
                      topRight: Radius.circular(40),
                      bottomRight: Radius.circular(40))),
              child: Text(
                "Buat Impianmu",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 17),
              ),
            ),
          )
        ],
      ),
    );
  }

  _listWidgetDreams() {
    return Expanded(
      child: Container(
        child: ListView.separated(
            shrinkWrap: true,
            itemBuilder: (context, index) {
              DreamsModel data = _listDataDreams[index];
              return _widgetItem(data);
            },
            separatorBuilder: (context, index) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Divider(color: MogeColors.primaryColor),
              );
            },
            itemCount: _listDataDreams.length),
      ),
    );
  }

  _widgetItem(DreamsModel data) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: InkWell(
        onTap: () async {
          bool? result = await Navigator.push(
              context,
              new MaterialPageRoute(
                builder: (context) => DetailDreamsPage(dreamsModel: data),
              ));

          if (result != null) {
            showFlutterToast("Edit Impian Berhasil");
          }

          await _getDataDreams();
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 70,
              height: 70,
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                  color: MogeColors.primaryColor, shape: BoxShape.circle),
              child: Container(
                decoration: BoxDecoration(
                    color: MogeColors.pale_grey,
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: !GlobalMethodHelper.isEmpty(data.imagePath)
                            ? MemoryImage(base64Decode(data.imagePath))
                            : AssetImage("assets/no_image.png")
                                as ImageProvider,
                        fit: BoxFit.fill)),
              ),
            ),
            SizedBox(width: 15),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  data.namaImpian,
                  style: TextStyle(
                      color: MogeColors.primaryColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 5),
                Text(
                  data.isActive == "1"
                      ? _countDifferentMonth(
                          DateTime.now(), data.tanggalPencapaian)
                      : "Pending",
                  style: TextStyle(
                      color: data.isActive == "1"
                          ? Color.fromRGBO(130, 150, 179, 1)
                          : Colors.red,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      FormatNumber.formatterLocal(data.danaTerkumpul),
                      style: TextStyle(
                          color: Color.fromRGBO(130, 150, 179, 1),
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(width: 5),
                    Text(
                      "dari",
                      style: TextStyle(
                          color: Color.fromRGBO(130, 150, 179, 1),
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    ),
                    SizedBox(width: 5),
                    Text(
                      FormatNumber.formatterLocal(data.danaPencapaian),
                      style: TextStyle(
                          color: Color.fromRGBO(130, 150, 179, 1),
                          fontSize: 15,
                          fontWeight: FontWeight.w500),
                    )
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  String _countDifferentMonth(DateTime dateNow, String datePencapaian) {
    String differentDays =
        dateNow.difference(DateTime.parse(datePencapaian)).inDays.toString();
    double differentMonth = (int.parse(differentDays) / 30);

    String result = differentMonth.round().toString().replaceAll("-", "");

    if (result == "0") {
      int days = int.parse(differentDays.replaceAll("-", "")) + 1;
      result = days.toString().replaceAll("-", "") + " Hari lagi";
    } else {
      result = result + " Bulan lagi";
    }

    return result;
  }
}
