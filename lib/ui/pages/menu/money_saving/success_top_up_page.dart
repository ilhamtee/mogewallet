import 'dart:async';

import 'package:flutter/material.dart';

class SuccessTopUpPage extends StatelessWidget {
  backFunction(BuildContext context) async {
    var duration = Duration(seconds: 2);
    return Timer(duration, () {
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    backFunction(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 130,
              height: 130,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.green, width: 4)),
              child: Icon(
                Icons.check,
                size: 90,
                color: Colors.green,
              ),
            ),
          ),
          SizedBox(height: 15),
          Center(
            child: Text(
              "Top Up \nSuccessful",
              style: TextStyle(
                  color: Colors.green,
                  fontSize: 25,
                  fontWeight: FontWeight.w500),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}
