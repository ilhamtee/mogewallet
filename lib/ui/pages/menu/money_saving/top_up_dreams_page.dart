import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/ui/pages/menu/money_saving/success_top_up_page.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/date_time_helper.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';
import 'package:moge_wallet/utils/services/dreams_service.dart';
import 'package:moge_wallet/utils/services/income_service.dart';

class TopUpDreamsPage extends StatefulWidget {
  final String typeTopUp;
  final DreamsModel dreamsModel;

  TopUpDreamsPage({required this.typeTopUp, required this.dreamsModel});

  @override
  _TopUpDreamsPageState createState() => _TopUpDreamsPageState();
}

class _TopUpDreamsPageState extends State<TopUpDreamsPage> {
  TextEditingController _nominalController = TextEditingController();
  TextEditingController _catatanController = TextEditingController();
  TextEditingController _noRekeningController = TextEditingController();

  FocusNode _focusNominal = FocusNode();
  FocusNode _focusCatatan = FocusNode();
  FocusNode _focusNoRekening = FocusNode();

  List<String> _listBank = [
    "Bank Indonesia",
    "Bank Jatim",
    "BNI",
    "BNI Syariah",
    "BRI",
    "BRI Syariah",
    "BTN",
    "BCA",
    "BCA Syariah",
    "CIMB NIAGA",
    "Mandiri"
  ];

  String _selectedBank = "BCA";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).padding.top,
            bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          children: <Widget>[
            _widgetAppBar(),
            Expanded(
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      _widgetBody(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: _buttonTopUp(),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: 40,
                    color: Colors.white,
                  )),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Top Up ${widget.typeTopUp}",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetBody() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.typeTopUp.contains("ATM")
              ? Center(
                  child: SvgPicture.asset(
                  "assets/icons/atm.svg",
                  width: 80,
                  colorFilter: ColorFilter.mode(
                    MogeColors.primaryColor,
                    BlendMode.srcIn,
                  ),
                ))
              : widget.typeTopUp.contains("Debit")
                  ? Center(
                      child: SvgPicture.asset(
                      "assets/icons/card.svg",
                      width: 80,
                      colorFilter: ColorFilter.mode(
                        MogeColors.primaryColor,
                        BlendMode.srcIn,
                      ),
                    ))
                  : Center(
                      child: SvgPicture.asset(
                      "assets/icons/mobile_banking.svg",
                      width: 80,
                      colorFilter: ColorFilter.mode(
                        MogeColors.primaryColor,
                        BlendMode.srcIn,
                      ),
                    )),
          SizedBox(height: 30),
          Text(
            "Pilih Bank",
            style: TextStyle(color: MogeColors.primaryColor),
          ),
          SizedBox(height: 3),
          Container(
            width: MediaQuery.of(context).size.width,
            child: DropdownButtonHideUnderline(
                child: DropdownButton(
              value: _selectedBank,
              isDense: true,
              isExpanded: true,
              icon: Icon(
                Icons.arrow_drop_down,
                color: MogeColors.primaryColor,
                size: 30,
              ),
              hint: Text(
                'Pilih Bank',
                style: TextStyle(color: MogeColors.primaryColor),
              ),
              onChanged: (String? newValue) {
                if (newValue != null) {
                  setState(() {
                    _selectedBank = newValue;
                  });
                }
              },
              items: _listBank.map((String value) {
                return DropdownMenuItem(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                );
              }).toList(),
            )),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Nomer Rekening",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _noRekeningController,
                focusNode: _focusNoRekening,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
          SizedBox(height: 5),
          Text(
            "Nominal Top Up (Rp)",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _nominalController,
                focusNode: _focusNominal,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
          SizedBox(height: 5),
          Text(
            "Catatan",
            style: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 17,
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 5),
              height: 50,
              padding:
                  EdgeInsets.only(right: 15, top: 10, bottom: 10, left: 15),
              child: TextField(
                controller: _catatanController,
                focusNode: _focusCatatan,
                style: TextStyle(color: MogeColors.primaryColor, fontSize: 18),
                cursorColor: MogeColors.primaryColor,
                decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: MogeColors.primaryColor)),
                    enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: MogeColors.primaryColor))),
              )),
        ],
      ),
    );
  }

  _buttonTopUp() {
    return Padding(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: InkWell(
        onTap: () {
          _doTopUp();
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
          height: 45,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: MogeColors.primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: Text(
            "Top Up",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
    );
  }

  _doTopUp() async {
    _focusNoRekening.unfocus();
    _focusCatatan.unfocus();
    _focusNominal.unfocus();
    if (_selectedBank.isNotEmpty &&
        _nominalController.text.isNotEmpty &&
        _noRekeningController.text.isNotEmpty) {
      
      DreamsModel dataDreams = widget.dreamsModel;

      HistoryModel dataIncome = HistoryModel(
          nama: "TopUp Dreams by " + _selectedBank,
          nominal:
              _nominalController.text.replaceAll(new RegExp(r"[^0-9]"), ""),
          date: DateTimeHelper.getDateFromCurrentDate(),
          type: "0",
          jenisRiwayat: "");

      await IncomeService.addIncome(dataIncome);

      int danaPencapaian = int.tryParse(
              _nominalController.text.replaceAll(new RegExp(r"[^0-9]"), "")) ??
          0 + (int.tryParse(dataDreams.danaTerkumpul) ?? 0);
      dataDreams.danaTerkumpul = danaPencapaian.toString();

      await DreamsService.updateDreams(widget.dreamsModel.id!, dataDreams);

      LoadingDialogWidget.showLoading(context, message: "Sedang Top Up ...");
      Future.delayed(Duration(seconds: 2), () async {
        _focusCatatan.unfocus();
        _focusNominal.unfocus();

        await Navigator.pushReplacement(
            context,
            new MaterialPageRoute(
              builder: (context) => SuccessTopUpPage(),
            ));

        _focusCatatan.unfocus();
        _focusNominal.unfocus();

        Future.delayed(Duration(seconds: 1), () async {
          Navigator.pop(context, dataDreams);
        });

        SystemChannels.textInput.invokeMethod('TextInput.hide');
      });
    } else {
      showFlutterToast("Lengkapi Form Top Up");
    }
  }
}
