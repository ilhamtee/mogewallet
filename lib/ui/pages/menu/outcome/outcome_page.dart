import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/ui/pages/scan_qr/scanner_page.dart';
import 'package:moge_wallet/ui/widgets/widget_menu_outcome.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class OutcomePage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  OutcomePage({required this.scaffoldState});

  @override
  _OutcomePageState createState() => _OutcomePageState();
}

class _OutcomePageState extends State<OutcomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: MogeColors.lightBlueColor.withOpacity(0.5),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[_widgetAppBar(), _widgetItemOutCome()],
          ),
          _widgetScanner()
        ],
      ),
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        color: MogeColors.primaryColor,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer()),
              alignment: Alignment.centerLeft,
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "Outcome",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontFamily: "MedioVintage"),
              ),
            )
          ],
        ));
  }

  _widgetItemOutCome() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Makanan",
                    assets: "assets/icons/makanan.svg",
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Kendaraan",
                    assets: "assets/icons/kendaraan.svg",
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Pendidikan",
                    assets: "assets/icons/pendidikan.svg",
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Kesehatan",
                    assets: "assets/icons/kesehatan.svg",
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 40, left: 20, right: 20),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Telepon",
                    assets: "assets/icons/telefon.svg",
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Belanja",
                    assets: "assets/icons/belanja.svg",
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  child: WidgetMenuOutcome(
                    title: "Asuransi",
                    assets: "assets/icons/asuransi.svg",
                  ),
                ),
              ),
              Expanded(flex: 1, child: Container())
            ],
          ),
        ),
      ],
    );
  }

  _widgetScanner() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: InkWell(
        onTap: () async {
          await Navigator.push(context, ScannerPage.route);
        },
        child: Container(
          width: 70,
          height: 70,
          margin: EdgeInsets.only(bottom: 20),
          decoration:
              BoxDecoration(color: Colors.white, shape: BoxShape.circle),
          child: Center(
            child: SvgPicture.asset(
              "assets/icons/scanner.svg",
              width: 40,
              colorFilter: ColorFilter.mode(
                MogeColors.primaryColor,
                BlendMode.srcIn,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
