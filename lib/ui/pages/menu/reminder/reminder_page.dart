import 'package:flutter/material.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';

class ReminderPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  ReminderPage({required this.scaffoldState});

  @override
  _ReminderPageState createState() => _ReminderPageState();
}

class _ReminderPageState extends State<ReminderPage> {
  List<String> _listTypeReminder = ["Harian", "Mingguan", "Bulanan"];
  TextEditingController _messageController = TextEditingController();
  FocusNode _focusMessage = FocusNode();

  int _indexListReminder = 0;
  bool _isAlarmActive = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _widgetAppBar(),
      body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                _widgetBackgroundHeader(),
                _widgetBodyReminder(),
              ],
            ),
          )),
      backgroundColor: MogeColors.lightBlueColor,
      bottomNavigationBar: _buttonContinue(),
    );
  }

  _widgetAppBar() {
    return PreferredSize(
        preferredSize: Size.fromHeight(40.0), // here the desired height
        child: AppBar(
          backgroundColor: MogeColors.primaryColor,
          centerTitle: true,
          elevation: 0,
          leading: Padding(
            padding: EdgeInsets.only(bottom: 10, left: 25),
            child: InkWell(
              onTap: () => widget.scaffoldState.currentState?.openDrawer(),
              child: Icon(
                Icons.menu,
                size: 30,
                color: Colors.white,
              ),
            ),
          ),
          title: Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Text(
              "Atur Limit Kamu",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontFamily: "MedioVintage"),
            ),
          ),
        ));
  }

  _widgetBodyReminder() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.80,
      padding: EdgeInsets.symmetric(vertical: 15),
      transform: Matrix4.translationValues(0.0, -100.0, 0.0),
      decoration: BoxDecoration(
          color: MogeColors.lightBlueColor,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(80), topLeft: Radius.circular(80))),
      child: Column(
        children: <Widget>[
          Container(
            width: 120,
            height: 3,
            color: MogeColors.primaryColor,
          ),
          SizedBox(height: 15),
          _widgetTypeReminder(),
          SizedBox(height: 15),
          _widgetLimit(),
          SizedBox(height: 15),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20))),
            margin: EdgeInsets.symmetric(horizontal: 40),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 40),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Alarm",
                        style: TextStyle(
                            color: MogeColors.primaryColor,
                            fontSize: 18,
                            fontWeight: FontWeight.w400),
                      ),
                      Switch(
                        value: _isAlarmActive,
                        onChanged: (value) {
                          setState(() {
                            _isAlarmActive = value;
                            print(_isAlarmActive);
                          });
                        },
                        activeTrackColor: MogeColors.darkBlueColor,
                        activeColor: MogeColors.primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Text(
                    "Custom Notifikasi",
                    style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontSize: 18,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(228, 233, 242, 1),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: TextField(
                      controller: _messageController,
                      focusNode: _focusMessage,
                      autofocus: false,
                      style: TextStyle(
                          color: MogeColors.primaryColor, fontSize: 15),
                      cursorColor: MogeColors.primaryColor,
                      maxLines: 5,
                      decoration: InputDecoration(
                        hintText: "Tulis Notifikasi Kamu Disini ...",
                        hintStyle: TextStyle(
                            color: MogeColors.disableTextGreyColor,
                            fontSize: 15),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _widgetBackgroundHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 140,
      color: MogeColors.primaryColor,
      alignment: Alignment.topCenter,
    );
  }

  _widgetTypeReminder() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      width: 220,
      decoration: BoxDecoration(
          color: MogeColors.darkBlueColor,
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if (_indexListReminder > 0) {
                setState(() {
                  _indexListReminder--;
                });
              }
            },
            child: Icon(
              Icons.keyboard_arrow_left,
              color: MogeColors.primaryColor,
              size: 40,
            ),
          ),
          Text(
            _listTypeReminder[_indexListReminder],
            style: TextStyle(
                color: MogeColors.primaryColor,
                fontSize: 20,
                fontWeight: FontWeight.w500),
          ),
          GestureDetector(
            onTap: () {
              if (_indexListReminder != 2) {
                setState(() {
                  _indexListReminder++;
                });
              }
            },
            child: Icon(
              Icons.keyboard_arrow_right,
              color: MogeColors.primaryColor,
              size: 40,
            ),
          ),
        ],
      ),
    );
  }

  _widgetLimit() {
    return Container(
      width: 270,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Limit",
            style: TextStyle(
                color: MogeColors.primaryColor,
                fontSize: 20,
                fontFamily: ConstantHelper.MEDIO_VINTAGE),
          ),
          Container(
            width: 150,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: MogeColors.darkBlueColor),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "RP.",
                  style: TextStyle(
                      color: MogeColors.primaryColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  "2.000.000",
                  style: TextStyle(
                      color: MogeColors.primaryColor,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _buttonContinue() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 80, vertical: 10),
      height: 50,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          color: MogeColors.primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: Text(
        "Simpan",
        style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
      ),
    );
  }
}
