import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/ui/pages/login/model/login_model.dart';
import 'package:moge_wallet/ui/pages/register/model/register_model.dart';
import 'package:moge_wallet/ui/widgets/input_textfield.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/services/register_service.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilePage extends StatefulWidget {
  final LoginModel dataUser;

  EditProfilePage({required this.dataUser});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _noHandphoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  FocusNode _focusName = FocusNode();
  FocusNode _focusHandphone = FocusNode();
  FocusNode _focusEmail = FocusNode();
  FocusNode _focusPassword = FocusNode();

  bool _hidePassword = true;

  @override
  void initState() {
    _setDataUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            _appBar(),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                    top: 30, bottom: MediaQuery.of(context).padding.bottom),
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SvgPicture.asset(
                          widget.dataUser.isParent == "1"
                              ? "assets/icons/mother_drawer.svg"
                              : "assets/icons/boy.svg",
                          width: 80),
                      SizedBox(height: 20),
                      InputTextField(
                        controller: _nameController,
                        focusNode: _focusName,
                        hintName: "NAMA",
                        isNumberOnly: false,
                        isPassword: false,
                        isEmail: false,
                      ),
                      InputTextField(
                        controller: _noHandphoneController,
                        focusNode: _focusHandphone,
                        hintName: "NOMOR PONSEL",
                        isNumberOnly: true,
                        isPassword: false,
                        isEmail: false,
                      ),
                      InputTextField(
                        controller: _emailController,
                        focusNode: _focusEmail,
                        hintName: "EMAIL",
                        isNumberOnly: false,
                        isPassword: false,
                        isEmail: true,
                      ),
                      _widgetPassword(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_bottomBar()],
      ),
    );
  }

  _appBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.keyboard_arrow_left,
                size: 50,
                color: MogeColors.primaryColor,
              )),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                "EDIT PROFILE",
                style: TextStyle(
                    color: MogeColors.blueColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
          )
        ],
      ),
    );
  }

  _bottomBar() {
    return InkWell(
      onTap: () async {
        _saveUser();
      },
      splashColor: MogeColors.lightBlueColor,
      child: Container(
        width: 200,
        height: 43,
        margin: EdgeInsets.symmetric(vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: MogeColors.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text(
          "SIMPAN",
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15),
        ),
      ),
    );
  }

  _widgetPassword() {
    return TextFormField(
      controller: _passwordController,
      focusNode: _focusPassword,
      style: TextStyle(
          color: MogeColors.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 14),
      autofocus: false,
      obscureText: _hidePassword,
      maxLines: 1,
      cursorColor: MogeColors.primaryColor,
      decoration: InputDecoration(
        hintText: "KATA SANDI",
        errorText: validatePassword(_passwordController.text),
        suffixIcon: IconButton(
            icon: Icon(
              !_hidePassword ? Icons.visibility : Icons.visibility_off,
              size: 25,
              color: MogeColors.primaryColor,
            ),
            onPressed: () {
              setState(() {
                _hidePassword = !_hidePassword;
              });
            }),
        hintStyle: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 14),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
      ),
    );
  }

  String? validatePassword(String value) {
    if (!(value.length >= 6) && value.isNotEmpty) {
      return "Minimal password 6 karakter";
    }
    return null;
  }

  _saveUser() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    RegisterModel user = RegisterModel(
        id: 1,
        nama: _nameController.text,
        no_handphone: _noHandphoneController.text,
        email: _emailController.text,
        password: _passwordController.text,
        isParent: widget.dataUser.isParent);
    _setUnfocus();

    if (_nameController.text.isNotEmpty &&
        _noHandphoneController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      await RegisterService.updateUser(widget.dataUser.id, user);
      LoadingDialogWidget.showLoading(context, message: "Update User ...");
      await sharedPref.setString(
          ConstantHelper.USER_NAME, _nameController.text);

      await Future.delayed(Duration(seconds: 2), () {
        _clearData();
        Navigator.pop(context);
        _setUnfocus();
        Navigator.pop(context, true);
      });
    } else {
      showFlutterToast("Mohon Lengkapi Form Pendaftaran");
    }
  }

  _clearData() {
    _nameController.clear();
    _noHandphoneController.clear();
    _noHandphoneController.clear();
    _emailController.clear();
    _passwordController.clear();
  }

  _setUnfocus() {
    _focusName.unfocus();
    _focusEmail.unfocus();
    _focusHandphone.unfocus();
    _focusPassword.unfocus();
  }

  _setDataUser() {
    _nameController.text = widget.dataUser.nama;
    _noHandphoneController.text = widget.dataUser.no_handphone;
    _emailController.text = widget.dataUser.email;
    _passwordController.text = widget.dataUser.password;
  }
}
