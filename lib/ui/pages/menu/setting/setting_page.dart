import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/ui/pages/login/login_page.dart';
import 'package:moge_wallet/ui/pages/login/model/login_model.dart';
import 'package:moge_wallet/ui/pages/menu/setting/edit_profile_page.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/services/login_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldState;

  SettingPage({required this.scaffoldState});

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  bool _isParent = false;
  List<LoginModel> _user = [];

  _getProfile() async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    List<LoginModel> _dataProfile = await LoginService.getAllUsers();
    setState(() {
      _user = _dataProfile
          .where((user) =>
              user.id ==
              int.parse(_sharedPref.getString(ConstantHelper.ID_USER) ?? '0'))
          .toList();
    });
    if (_user.first.isParent == "1") {
      setState(() {
        _isParent = true;
      });
    }
  }

  @override
  void initState() {
    _getProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
            child: Container(
          color: MogeColors.primaryColor,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _widgetAppBar(),
              Expanded(
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _headerProfile(),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          decoration: BoxDecoration(
                              color: MogeColors.lightBlueColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "assets/icons/setting_phone.svg",
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                    width: 30,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    _user.isNotEmpty
                                        ? "${_user.first.no_handphone}"
                                        : "",
                                    style: TextStyle(
                                        color: MogeColors.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                              SizedBox(height: 15),
                              Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "assets/icons/setting_email.svg",
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                    width: 30,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    _user.isNotEmpty
                                        ? "${_user.first.email}"
                                        : "",
                                    style: TextStyle(
                                        color: MogeColors.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          decoration: BoxDecoration(
                              color: MogeColors.lightBlueColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "assets/icons/setting_notif.svg",
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                    width: 30,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    "Notification",
                                    style: TextStyle(
                                        color: MogeColors.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          decoration: BoxDecoration(
                              color: MogeColors.lightBlueColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "assets/icons/setting_privacy.svg",
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                    width: 30,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    "Privacy & Security",
                                    style: TextStyle(
                                        color: MogeColors.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          decoration: BoxDecoration(
                              color: MogeColors.lightBlueColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    "assets/icons/setting_bank.svg",
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                    width: 30,
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    "Bank Account",
                                    style: TextStyle(
                                        color: MogeColors.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        )),
      ],
    );
  }

  _widgetAppBar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 60,
        alignment: Alignment.bottomLeft,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: IconButton(
                  icon: Icon(
                    Icons.menu,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      widget.scaffoldState.currentState?.openDrawer()),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: IconButton(
                  icon: Icon(
                    Icons.exit_to_app,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: () => _showDialogLogout()),
            ),
          ],
        ));
  }

  _headerProfile() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SvgPicture.asset(
              _isParent
                  ? "assets/icons/mother_drawer.svg"
                  : "assets/icons/boy.svg",
              width: 80),
          SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "${_user.isNotEmpty ? _user.first.nama.toUpperCase() : ""}",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              SizedBox(height: 10),
              InkWell(
                onTap: () async {
                  bool? result = await Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => EditProfilePage(
                          dataUser: _user.first,
                        ),
                      ));

                  if (result != null || result == true) {
                    _getProfile();
                  }
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(117, 160, 230, 1),
                      borderRadius: BorderRadius.all(Radius.circular(40))),
                  child: Text(
                    "Edit Profile",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  _showDialogLogout() {
    AlertDialog alert = AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text("Logout"),
      content: Text("Anda yakin untuk logout?"),
      actions: [
        ElevatedButton(
            child: Text(
              "TIDAK",
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () => Navigator.of(context).pop()),
        ElevatedButton(
            child: Text("YA", style: TextStyle(color: MogeColors.blueColor)),
            onPressed: () => _doLogOut()),
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _doLogOut() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    sharedPref.remove(ConstantHelper.ID_USER);
    sharedPref.remove(ConstantHelper.USER_NAME);
    NavigateHelpers.pushAndRemove(context, LoginPage());
  }
}
