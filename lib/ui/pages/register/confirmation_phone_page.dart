import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/ui/widgets/custom_pin_input.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class ConfirmationPhonePage extends StatefulWidget {
  @override
  _ConfirmationPhonePageState createState() => _ConfirmationPhonePageState();
}

class _ConfirmationPhonePageState extends State<ConfirmationPhonePage> {
  int _startTimer = 10;

  _startTimerFunction() {
    Future.delayed(Duration(seconds: 1), () {
      if (_startTimer > 0) {
        if (mounted) {
          setState(() {
            _startTimer = _startTimer - 1;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _startTimerFunction();
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.bottomCenter,
              child: SvgPicture.asset(
                "assets/icons/mail_send.svg",
                width: 100,
                colorFilter: ColorFilter.mode(
                  MogeColors.primaryColor,
                  BlendMode.srcIn,
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                children: <Widget>[
                  RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                        text: "Masukkan Kode",
                        style: TextStyle(
                            color: MogeColors.primaryColor, fontSize: 15),
                        children: [
                          TextSpan(
                            text: " OTP",
                            style: TextStyle(
                                color: MogeColors.primaryColor,
                                fontSize: 15,
                                fontWeight: FontWeight.bold),
                          )
                        ]),
                  ),
                  SizedBox(height: 20),
                  CustomPinInput(
                    cursorColor: MogeColors.primaryColor,
                    boxColor: Colors.white,
                    showFieldAsBox: true,
                    isTextObscure: false,
                    textColor: MogeColors.primaryColor,
                    fontSize: 20,
                    onSubmit: (String value) {},
                  ),
                  SizedBox(height: 20),
                  Text(
                    "00 : $_startTimer",
                    style:
                        TextStyle(color: MogeColors.primaryColor, fontSize: 17),
                  ),
                  SizedBox(height: 10),
                  RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                        text: "Belum Menerima Kode?",
                        style: TextStyle(
                            color: MogeColors.primaryColor, fontSize: 15),
                        children: [
                          TextSpan(
                              text: " Kirim Lagi",
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  if (_startTimer == 0) {
                                    setState(() {
                                      _startTimer = 10;
                                    });
                                  }
                                },
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: _startTimer > 0
                                      ? MogeColors.darkGreyColor
                                      : MogeColors.primaryColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ]),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
