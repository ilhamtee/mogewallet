import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/home/home_page.dart';
import 'package:moge_wallet/ui/widgets/slide_dots.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int _currentPage = 0;
  final PageController _pageController = PageController(initialPage: 0);

  _onChangePage(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topCenter,
                      child: PageView.builder(
                        itemCount: 4,
                        itemBuilder: (context, i) =>
                            showSlide(context, _currentPage),
                        physics: AlwaysScrollableScrollPhysics(),
                        onPageChanged: _onChangePage,
                        controller: _pageController,
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, right: 10),
                        child: InkWell(
                          onTap: () {
                            NavigateHelpers.pushAndRemove(context, HomePage());
                          },
                          splashColor: Colors.transparent,
                          child: Text(
                            _currentPage == 3 ? "SELANJUTNYA" : "SKIP",
                            style: TextStyle(
                                color: MogeColors.primaryColor,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: EdgeInsets.only(bottom: 100),
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              for (int i = 0; i < 4; i++)
                                if (i == _currentPage)
                                  SlideDots(true)
                                else
                                  SlideDots(false)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showSlide(BuildContext context, int index) {
    switch (index) {
      case 0:
        return slideOne();
      case 1:
        return slideTwo();
      case 2:
        return slideThree();
      case 3:
        return slideFour();

      default:
        return SizedBox.shrink();
    }
  }

  Widget slideOne() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/landing_image/landing1.jpg"),
                    fit: BoxFit.fill)),
          ),
        )
      ],
    );
  }

  Widget slideTwo() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/landing_image/landing2.jpg"),
                    fit: BoxFit.fill)),
          ),
        )
      ],
    );
  }

  Widget slideThree() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/landing_image/landing3.jpg"),
                    fit: BoxFit.fill)),
          ),
        )
      ],
    );
  }

  Widget slideFour() {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/landing_image/landing4.jpg"),
                    fit: BoxFit.fill)),
          ),
        )
      ],
    );
  }
}
