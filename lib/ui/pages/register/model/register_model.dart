class RegisterModel {
  final int? id;
  final String nama;
  final String no_handphone;
  final String email;
  final String password;
  final String isParent;

  RegisterModel({
    this.id,
    required this.nama,
    required this.no_handphone,
    required this.email,
    required this.password,
    required this.isParent,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "nama": nama,
      "no_handphone": no_handphone,
      "email": email,
      "password": password,
      "isParent": isParent
    };
  }
}
