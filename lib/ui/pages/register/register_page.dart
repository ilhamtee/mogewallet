import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moge_wallet/models/dropdown_roles_model.dart';
import 'package:moge_wallet/ui/pages/register/model/register_model.dart';
import 'package:moge_wallet/ui/widgets/input_textfield.dart';
import 'package:moge_wallet/ui/widgets/loading_dialog_widget.dart';
import 'package:moge_wallet/ui/widgets/widget_toast.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:moge_wallet/utils/services/register_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'confirmation_phone_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _noHandphoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _passConfirmationController = TextEditingController();

  FocusNode _focusName = FocusNode();
  FocusNode _focusHandphone = FocusNode();
  FocusNode _focusEmail = FocusNode();
  FocusNode _focusPassword = FocusNode();
  FocusNode _focusPassConfirm = FocusNode();

  bool _hidePasswordConfrim = true;
  bool _hidePassword = true;

  List<RolesModel> _listRolesModel = [
    RolesModel(id: "1", rolesName: "Orang Tua"),
    RolesModel(id: "0", rolesName: "Anak")
  ];

  String? _selectedRoles;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: <Widget>[
            _appBar(),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                    top: 30, bottom: MediaQuery.of(context).padding.bottom),
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      InputTextField(
                        controller: _nameController,
                        focusNode: _focusName,
                        hintName: "NAMA",
                        isNumberOnly: false,
                        isPassword: false,
                        isEmail: false,
                      ),
                      InputTextField(
                        controller: _noHandphoneController,
                        focusNode: _focusHandphone,
                        hintName: "NOMOR PONSEL",
                        isNumberOnly: true,
                        isPassword: false,
                        isEmail: false,
                      ),
                      InputTextField(
                        controller: _emailController,
                        focusNode: _focusEmail,
                        hintName: "EMAIL",
                        isNumberOnly: false,
                        isPassword: false,
                        isEmail: true,
                      ),
                      _widgetPassword(),
                      SizedBox(
                        height: 20,
                      ),
                      _widgetConfirmPassword(),
                      _widgetChooseRole()
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_bottomBar()],
      ),
    );
  }

  _appBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          InkWell(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.keyboard_arrow_left,
                size: 50,
                color: MogeColors.primaryColor,
              )),
          Expanded(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 20),
              child: Text(
                "REGISTER",
                style: TextStyle(
                    color: MogeColors.blueColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
          )
        ],
      ),
    );
  }

  _bottomBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      child: InkWell(
        onTap: () async {
          _saveUser();
        },
        splashColor: MogeColors.lightBlueColor,
        child: Row(
          textDirection: TextDirection.rtl,
          children: <Widget>[
            Icon(
              Icons.keyboard_arrow_right,
              size: 30,
              color: MogeColors.primaryColor,
            ),
            SizedBox(width: 15),
            Text(
              "DAFTAR",
              style: TextStyle(
                  color: MogeColors.primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }

  _widgetChooseRole() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 30,
        ),
        Text(
          "Roles",
          style: TextStyle(color: MogeColors.primaryColor),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: DropdownButtonHideUnderline(
              child: DropdownButton(
            value: _selectedRoles,
            isDense: true,
            isExpanded: true,
            icon: Icon(
              Icons.arrow_drop_down,
              color: MogeColors.primaryColor,
              size: 30,
            ),
            hint: Text(
              'Pilih Roles',
              style: TextStyle(color: MogeColors.primaryColor),
            ),
            onChanged: (String? newValue) {
              if (newValue != null) {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                setState(() {
                  _selectedRoles = newValue;
                });
                SystemChannels.textInput.invokeMethod('TextInput.hide');
              }
            },
            items: _listRolesModel.map((RolesModel value) {
              return DropdownMenuItem(
                value: value.id,
                child: Text(
                  value.rolesName,
                  style: TextStyle(
                      color: MogeColors.primaryColor,
                      fontWeight: FontWeight.bold),
                ),
              );
            }).toList(),
          )),
        ),
      ],
    );
  }

  _widgetPassword() {
    return TextFormField(
      controller: _passwordController,
      focusNode: _focusPassword,
      style: TextStyle(
          color: MogeColors.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 14),
      autofocus: false,
      obscureText: _hidePassword,
      maxLines: 1,
      cursorColor: MogeColors.primaryColor,
      decoration: InputDecoration(
        hintText: "KATA SANDI",
        errorText: validatePassword(_passwordController.text),
        suffixIcon: IconButton(
            icon: Icon(
              !_hidePassword ? Icons.visibility : Icons.visibility_off,
              size: 25,
              color: MogeColors.primaryColor,
            ),
            onPressed: () {
              setState(() {
                _hidePassword = !_hidePassword;
              });
            }),
        hintStyle: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 14),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
      ),
    );
  }

  _widgetConfirmPassword() {
    return TextFormField(
      controller: _passConfirmationController,
      focusNode: _focusPassConfirm,
      style: TextStyle(
          color: MogeColors.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 14),
      autofocus: false,
      obscureText: _hidePasswordConfrim,
      maxLines: 1,
      cursorColor: MogeColors.primaryColor,
      decoration: InputDecoration(
        hintText: "KONFIRMASI KATA SANDI",
        errorText: validateConfirmPassword(_passConfirmationController.text),
        suffixIcon: IconButton(
            icon: Icon(
              !_hidePasswordConfrim ? Icons.visibility : Icons.visibility_off,
              size: 25,
              color: MogeColors.primaryColor,
            ),
            onPressed: () {
              setState(() {
                _hidePasswordConfrim = !_hidePasswordConfrim;
              });
            }),
        hintStyle: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 14),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MogeColors.primaryColor),
        ),
      ),
    );
  }

  String? validateConfirmPassword(String value) {
    if (!(value.length >= 6) && value.isNotEmpty) {
      return "Minimal password 6 karakter";
    } else if (_passwordController.text != _passConfirmationController.text) {
      return "Konfirmasi kata sandi tidak sama";
    }
    return null;
  }

  String? validatePassword(String value) {
    if (!(value.length >= 6) && value.isNotEmpty) {
      return "Minimal password 6 karakter";
    }
    return null;
  }

  _saveUser() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    _setUnfocus();

    if (_nameController.text.isNotEmpty &&
        _noHandphoneController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _passConfirmationController.text.isNotEmpty &&
        _selectedRoles != null) {
      RegisterModel user = RegisterModel(
          id: 1,
          nama: _nameController.text,
          no_handphone: _noHandphoneController.text,
          email: _emailController.text,
          password: _passwordController.text,
          isParent: _selectedRoles!);
      int result = await RegisterService.addUser(user);

      LoadingDialogWidget.showLoading(context,
          message: "Mendaftarkan User ...");

      await sharedPref.setString(ConstantHelper.ID_USER, result.toString());
      await sharedPref.setString(
          ConstantHelper.USER_NAME, _nameController.text);
      await sharedPref.setBool(ConstantHelper.IS_LOGIN, true);

      await Future.delayed(Duration(seconds: 2), () {
        _clearData();
        Navigator.pop(context);
        NavigateHelpers.pushAndRemove(context, ConfirmationPhonePage());
      });
    } else {
      showFlutterToast("Mohon Lengkapi Form Pendaftaran");
    }
  }

  _clearData() {
    _nameController.clear();
    _noHandphoneController.clear();
    _noHandphoneController.clear();
    _emailController.clear();
    _passwordController.clear();
    _passConfirmationController.clear();
  }

  _setUnfocus() {
    _focusName.unfocus();
    _focusEmail.unfocus();
    _focusHandphone.unfocus();
    _focusPassword.unfocus();
    _focusPassConfirm.unfocus();
  }
}
