import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/ui/pages/scan_qr/scanner_page.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class ScanQRPage extends StatefulWidget {
  @override
  _ScanQRPageState createState() => _ScanQRPageState();
}

class _ScanQRPageState extends State<ScanQRPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[_widgetLogoQR(), _widgetScanner()],
        ),
      ),
    );
  }

  _widgetLogoQR() {
    return Align(
      alignment: Alignment.center,
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: SvgPicture.asset(
          "assets/icons/qr_code.svg",
          width: 200,
          colorFilter: ColorFilter.mode(
            MogeColors.primaryColor,
            BlendMode.srcIn,
          ),
        ),
      ),
    );
  }

  _widgetScanner() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: InkWell(
        onTap: () async {
          Navigator.push(context, ScannerPage.route);
        },
        child: Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: SvgPicture.asset(
            "assets/icons/scanner.svg",
            width: 40,
            colorFilter: ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
      ),
    );
  }
}
