import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/home/home_page.dart';
import 'package:moge_wallet/ui/pages/login/login_page.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    _startSplashScreen();
    super.initState();
  }

  void _startSplashScreen() async {
    SharedPreferences sharedPref = await SharedPreferences.getInstance();
    await InitDB.database;
    var _randomString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

    Random _rnd = Random();

    String getRandomString(int length) => String.fromCharCodes(
          Iterable.generate(
            length,
            (_) => _randomString.codeUnitAt(
              _rnd.nextInt(_randomString.length),
            ),
          ),
        );
    await sharedPref.setString(
        ConstantHelper.CODE_REFERRAL, getRandomString(7));
    String? idUser = sharedPref.getString(ConstantHelper.ID_USER);
    bool? isLogin = sharedPref.getBool(ConstantHelper.IS_LOGIN);

    final duration = Duration(seconds: 3);
    await Future.delayed(duration, () {
      if (idUser != null || isLogin == true || isLogin != null) {
        NavigateHelpers.pushAndRemove(context, HomePage());
      } else {
        NavigateHelpers.pushAndRemove(context, LoginPage());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MogeColors.lightBlueColor,
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/splash.png"), fit: BoxFit.fill)),
        alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.only(bottom: 10),
          child: Image.asset(
            "assets/icons/loading.gif",
            width: 70,
          ),
        ),
      ),
    );
  }
}
