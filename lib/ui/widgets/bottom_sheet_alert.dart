import 'package:flutter/material.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class BottomSheetAlert {
  final String title;
  final String message;
  final String primaryButtonText;
  final String secondaryButtonText;
  final VoidCallback? onPrimaryButtonClick;
  final VoidCallback? onSecondaryButtonClick;
  final Color? primaryButtonColor;

  BottomSheetAlert({
    required this.title,
    required this.message,
    required this.primaryButtonText,
    required this.secondaryButtonText,
    required this.onPrimaryButtonClick,
    this.onSecondaryButtonClick,
    this.primaryButtonColor,
  });

  void show(BuildContext context) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: const Radius.circular(10),
            topRight: const Radius.circular(10),
          ),
        ),
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(24, 20, 24, 20),
            decoration: BoxDecoration(
              color: Colors.transparent,
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(10),
                topRight: const Radius.circular(10),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Text(
                        title,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                          alignment: Alignment.centerRight,
                          child: IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          )),
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Text(
                  message,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.normal),
                ),
                SizedBox(height: 47),
                // Action Button
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                        child: GestureDetector(
                      onTap: onSecondaryButtonClick,
                      child: Container(
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            border: Border(
                              bottom: BorderSide(
                                  color: MogeColors.lightGreyColor, width: 1),
                              top: BorderSide(
                                  color: MogeColors.lightGreyColor, width: 1),
                              left: BorderSide(
                                  color: MogeColors.lightGreyColor, width: 1),
                              right: BorderSide(
                                  color: MogeColors.lightGreyColor, width: 1),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        margin: EdgeInsets.only(right: 3),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: Text(
                          secondaryButtonText,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: MogeColors.redColor,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    )),
                    Expanded(
                        child: GestureDetector(
                      onTap: onPrimaryButtonClick,
                      child: Container(
                        decoration: new BoxDecoration(
                            color:
                                primaryButtonColor ?? MogeColors.primaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        margin: EdgeInsets.only(left: 3),
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              primaryButtonText,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                    ))
                  ],
                ),
              ],
            ),
          );
        });
  }
}
