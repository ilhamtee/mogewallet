import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moge_wallet/ui/pages/register/landing_page.dart';
import 'package:moge_wallet/utils/helper/navigate_helper.dart';

class CustomPinInput extends StatefulWidget {
  final String? lastPin;
  final int fields;
  final ValueChanged<String> onSubmit;
  final double fieldWidth;
  final double fontSize;
  final bool isTextObscure;
  final bool showFieldAsBox;
  final cursorColor; // Leaving the data type dynamic for adding Color or Material Color
  final boxColor;
  final textColor;

  CustomPinInput({
    required this.onSubmit,
    this.lastPin,
    this.fields = 4,
    this.fieldWidth = 50.0,
    this.fontSize = 20.0,
    this.isTextObscure = false,
    this.showFieldAsBox = false,
    this.cursorColor = Colors.blue,
    this.boxColor = Colors.blue,
    this.textColor = Colors.blue,
  }) : assert(fields > 0);

  @override
  State createState() {
    return CustomPinInputState();
  }
}

class CustomPinInputState extends State<CustomPinInput> {
  late List<String?> _pin;
  late List<FocusNode?> _focusNodes;
  late List<TextEditingController?> _textControllers;

  Widget textFields = Container();

  @override
  void initState() {
    super.initState();
    _pin = List.generate(widget.fields, (index) => '');
    _focusNodes = List.generate(widget.fields, (index) => FocusNode());
    _textControllers =
        List.generate(widget.fields, (index) => TextEditingController());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        if (widget.lastPin != null) {
          for (int i = 0; i < widget.lastPin!.length; i++) {
            _pin[i] = widget.lastPin![i];
          }
        }
        textFields = generateTextFields(context);
      });
    });
  }

  @override
  void dispose() {
    _textControllers.forEach((TextEditingController? t) => t?.dispose());
    super.dispose();
  }

  Widget generateTextFields(BuildContext context) {
    List<Widget> textFields = List.generate(widget.fields, (int i) {
      return buildTextField(i, context, i == 0);
    });

    if (_pin.first?.isNotEmpty == true) {
      FocusScope.of(context).requestFocus(_focusNodes[0]);
    }

    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        verticalDirection: VerticalDirection.down,
        children: textFields);
  }

  void clearTextFields() {
    _textControllers.forEach(
        (TextEditingController? tEditController) => tEditController?.clear());
    _pin.clear();
  }

  Widget buildTextField(int i, BuildContext context, [bool autofocus = false]) {
    if (_focusNodes[i] == null) {
      _focusNodes[i] = FocusNode();
    }
    if (_textControllers[i] == null) {
      _textControllers[i] = TextEditingController();
      if (widget.lastPin != null) {
        _textControllers[i]?.text = widget.lastPin?[i] ?? '';
      }
    }

    _focusNodes[i]?.addListener(() {
      if (_focusNodes[i]?.hasFocus == true) {}
    });

    final String? lastDigit = _textControllers[i]?.text;

    return Container(
      width: widget.fieldWidth,
      height: 55,
      margin: EdgeInsets.only(right: 10.0),
      color: Colors.white,
      child: TextField(
        controller: _textControllers[i],
        keyboardType: TextInputType.number,
        textAlign: TextAlign.center,
        cursorColor: widget.cursorColor,
        maxLength: 1,
        autofocus: autofocus,
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
        ],
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: widget.textColor,
            // color: Colors.black,
            fontSize: widget.fontSize),
        focusNode: _focusNodes[i],
        obscureText: widget.isTextObscure,
        decoration: InputDecoration(
            counterText: "",
            enabledBorder: widget.showFieldAsBox
                ? OutlineInputBorder(
                    borderSide: BorderSide(width: 1.0, color: widget.boxColor))
                : null,
            focusedBorder: widget.showFieldAsBox
                ? OutlineInputBorder(
                    borderSide: BorderSide(width: 2.0, color: widget.boxColor))
                : null),
        onChanged: (String str) async {
          setState(() {
            _pin[i] = str;
          });
          if (i + 1 != widget.fields) {
            _focusNodes[i]?.unfocus();
            if (lastDigit != null && _pin[i] == '') {
              FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
            } else {
              FocusScope.of(context).requestFocus(_focusNodes[i + 1]);
            }
          } else {
            _focusNodes[i]?.unfocus();
            if (lastDigit != null && _pin[i] == '') {
              FocusScope.of(context).requestFocus(_focusNodes[i - 1]);
            }
          }
          if (_pin.every((String? digit) => digit != null && digit != '')) {
            await Future.delayed(Duration(seconds: 1), () {
              NavigateHelpers.push(context, LandingPage());
            });

            widget.onSubmit(_pin.join());
          }
        },
        onSubmitted: (String str) {
          if (_pin.every((String? digit) => digit != null && digit != '')) {
            widget.onSubmit(_pin.join());
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return textFields;
  }
}
