import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/menu/dashboard/model/chart_wallet_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class DailyChartWidget extends StatelessWidget {
  const DailyChartWidget({super.key});

  static final List<ChartWalletModel> _daysWalletData = [
    ChartWalletModel(money: 30000, day: '4'),
    ChartWalletModel(money: 45000, day: '7'),
    ChartWalletModel(money: 80000, day: '12'),
    ChartWalletModel(money: 50000, day: '14'),
    ChartWalletModel(money: 40000, day: '19'),
    ChartWalletModel(money: 90000, day: '23'),
    ChartWalletModel(money: 75000, day: '28')
  ];

  @override
  Widget build(BuildContext context) {
    return BarChart(
      BarChartData(
          titlesData: FlTitlesData(
            show: true,
            rightTitles: const AxisTitles(
              sideTitles: SideTitles(showTitles: false),
            ),
            topTitles: const AxisTitles(
              sideTitles: SideTitles(showTitles: false),
              axisNameWidget: Text(
                'Bulan Februari',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              axisNameSize: 30,
            ),
            bottomTitles: AxisTitles(
              sideTitles: SideTitles(
                showTitles: true,
                getTitlesWidget: getDaysBottomTitles,
                reservedSize: 30,
              ),
              axisNameWidget: Text(
                'Hari ke-',
              ),
            ),
          ),
          borderData: FlBorderData(
            show: false,
          ),
          barTouchData: barTouchData,
          gridData: const FlGridData(show: false),
          barGroups: List.generate(
            _daysWalletData.length,
            (index) => BarChartGroupData(
              x: index,
              showingTooltipIndicators: [0],
              barRods: [
                BarChartRodData(
                  toY: _daysWalletData[index].money.toDouble(),
                  color: MogeColors.primaryColor,
                  width: 25,
                  borderRadius: BorderRadius.zero,
                ),
              ],
            ),
          ).toList()),
    );
  }

  BarTouchData get barTouchData => BarTouchData(
        enabled: false,
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipPadding: EdgeInsets.zero,
          tooltipMargin: 0.0,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            return BarTooltipItem(
              '${(rod.toY.round() / 1000).toStringAsFixed(0)}K',
              TextStyle(
                color: MogeColors.primaryColor,
                fontWeight: FontWeight.bold,
              ),
            );
          },
        ),
      );

  Widget getDaysBottomTitles(double value, TitleMeta meta) {
    final style = TextStyle(
      color: MogeColors.primaryColor,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );

    return SideTitleWidget(
      axisSide: meta.axisSide,
      fitInside: SideTitleFitInsideData.disable(),
      child: Text(
        _daysWalletData[value.toInt()].day,
        style: style,
      ),
    );
  }
}
