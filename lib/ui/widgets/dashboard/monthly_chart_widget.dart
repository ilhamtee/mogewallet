import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/menu/dashboard/model/chart_wallet_model.dart';
import 'package:moge_wallet/ui/widgets/indicator.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';

class MonthlyChartWidget extends StatefulWidget {
  const MonthlyChartWidget({super.key});

  @override
  State<MonthlyChartWidget> createState() => _MonthlyChartWidgetState();
}

class _MonthlyChartWidgetState extends State<MonthlyChartWidget> {
  int touchedIndex = -1;

  final List<ChartWalletModel> _monthsWalletData = [
    ChartWalletModel(money: 370000, day: "Nov 2022"),
    ChartWalletModel(money: 250000, day: "Des 2023"),
    ChartWalletModel(money: 290000, day: "Jan 2024"),
  ];

  final List<Color> _monthColorsData = [
    Colors.amber,
    Colors.cyan,
    Colors.green,
  ];

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: PieChart(
            PieChartData(
              pieTouchData: PieTouchData(
                touchCallback: (FlTouchEvent event, pieTouchResponse) {
                  setState(() {
                    if (!event.isInterestedForInteractions ||
                        pieTouchResponse == null ||
                        pieTouchResponse.touchedSection == null) {
                      touchedIndex = -1;
                      return;
                    }
                    touchedIndex =
                        pieTouchResponse.touchedSection!.touchedSectionIndex;
                  });
                },
              ),
              borderData: FlBorderData(
                show: false,
              ),
              sections: _monthsWalletData.map((e) {
                int i = _monthsWalletData.indexOf(e);
                final isTouched = i == touchedIndex;
                final fontSize = isTouched ? 20.0 : 16.0;
                final radius = isTouched ? 130.0 : 120.0;
                const shadows = [Shadow(color: Colors.black, blurRadius: 2)];
                return PieChartSectionData(
                  color: _monthColorsData[i],
                  value: e.money.toDouble(),
                  title:
                      'Rp ${FormatNumber.formatterLocal(e.money.toString())}',
                  radius: radius,
                  titleStyle: TextStyle(
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    shadows: shadows,
                  ),
                );
              }).toList(),
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _monthsWalletData
              .map(
                (e) => Indicator(
                  color: _monthColorsData[_monthsWalletData.indexOf(e)],
                  text: e.day,
                  isSquare: true,
                  size: 12,
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
