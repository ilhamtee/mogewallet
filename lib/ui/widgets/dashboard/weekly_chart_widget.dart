import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:moge_wallet/ui/pages/menu/dashboard/model/chart_wallet_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/helper/number_format.dart';

class WeeklyChartWidget extends StatelessWidget {
  const WeeklyChartWidget({super.key});

  static final List<ChartWalletModel> _weeksWalletData = [
    ChartWalletModel(money: 100000, day: "Minggu 1"),
    ChartWalletModel(money: 140000, day: "Minggu 2"),
    ChartWalletModel(money: 130000, day: "Minggu 3"),
    ChartWalletModel(money: 170000, day: "Minggu 4"),
  ];

  @override
  Widget build(BuildContext context) {
    return LineChart(
      LineChartData(
        titlesData: FlTitlesData(
          rightTitles: const AxisTitles(
            sideTitles: SideTitles(showTitles: false),
            axisNameWidget: Text('Total Pengeluaran'),
            axisNameSize: 20,
          ),
          topTitles: AxisTitles(
            sideTitles: SideTitles(showTitles: false),
            axisNameWidget: Text(
              'Bulan Februari 2024',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
            axisNameSize: 30,
          ),
          bottomTitles: AxisTitles(
            sideTitles: SideTitles(
              showTitles: true,
              reservedSize: 25,
              interval: 1,
              getTitlesWidget: getWeeksBottomTitles,
            ),
            axisNameWidget: Text(
              'Minggu ke-',
            ),
            axisNameSize: 30,
          ),
        ),
        lineTouchData: LineTouchData(
          enabled: true,
          touchTooltipData: LineTouchTooltipData(
            tooltipBgColor: MogeColors.primaryColor.withOpacity(0.8),
            tooltipRoundedRadius: 8,
            getTooltipItems: (List<LineBarSpot> lineBarsSpot) {
              return lineBarsSpot.map((lineBarSpot) {
                return LineTooltipItem(
                  'Rp ' + FormatNumber.formatterLocal(lineBarSpot.y.toString()),
                  const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                );
              }).toList();
            },
          ),
        ),
        lineBarsData: [
          LineChartBarData(
            showingIndicators:
                List.generate(_weeksWalletData.length, (index) => index)
                    .toList(),
            spots: List.generate(
              _weeksWalletData.length,
              (index) => FlSpot(
                index.toDouble(),
                _weeksWalletData[index].money.toDouble(),
              ),
            ).toList(),
            isCurved: true,
            barWidth: 3,
            dotData: FlDotData(
              getDotPainter: (spot, percent, barData, index) {
                return FlDotCirclePainter(
                  radius: 5,
                  color: MogeColors.primaryColor,
                );
              },
            ),
            color: MogeColors.primaryColor,
            belowBarData: BarAreaData(
              show: true,
              color: MogeColors.lightBlueColor,
            ),
          ),
        ],
        borderData: FlBorderData(
          border: Border(
            left: BorderSide(
              color: MogeColors.primaryColor,
            ),
            bottom: BorderSide(
              color: MogeColors.primaryColor,
            ),
          ),
        ),
      ),
    );
  }

  Widget getWeeksBottomTitles(double value, TitleMeta meta) {
    final style = TextStyle(
      color: MogeColors.primaryColor,
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );

    return SideTitleWidget(
      axisSide: meta.axisSide,
      fitInside: SideTitleFitInsideData.disable(),
      child: Text(
        _weeksWalletData[value.toInt()].day.toString().replaceAll('Minggu', ''),
        style: style,
      ),
    );
  }
}
