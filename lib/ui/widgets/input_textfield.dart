import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class InputTextField extends StatefulWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final String hintName;
  final bool isNumberOnly;
  final bool isEmail;
  final bool isPassword;

  InputTextField({
    required this.controller,
    this.focusNode,
    required this.hintName,
    this.isNumberOnly = false,
    this.isEmail = false,
    this.isPassword = false,
  });

  @override
  _InputTextFieldState createState() => _InputTextFieldState();
}

class _InputTextFieldState extends State<InputTextField> {
  late bool _hidePassword;

  @override
  void initState() {
    _hidePassword = widget.isPassword;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: TextField(
        controller: widget.controller,
        focusNode: widget.focusNode,
        style: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 14),
        autofocus: false,
        obscureText: _hidePassword,
        maxLines: 1,
        keyboardType: widget.isNumberOnly
            ? TextInputType.number
            : widget.isEmail
                ? TextInputType.emailAddress
                : TextInputType.text,
        inputFormatters: widget.isNumberOnly
            ? [
                FilteringTextInputFormatter.digitsOnly,
              ]
            : null,
        maxLength: widget.isNumberOnly ? 12 : 100,
        cursorColor: MogeColors.primaryColor,
        textCapitalization: widget.isEmail || widget.isNumberOnly
            ? TextCapitalization.none
            : TextCapitalization.words,
        decoration: InputDecoration(
          hintText: widget.hintName,
          counterText: '',
          suffixIcon: widget.isPassword
              ? IconButton(
                  icon: Icon(
                    !_hidePassword ? Icons.visibility : Icons.visibility_off,
                    size: 25,
                    color: MogeColors.primaryColor,
                  ),
                  onPressed: () {
                    setState(() {
                      _hidePassword = !_hidePassword;
                    });
                  })
              : null,
          hintStyle: TextStyle(
              color: MogeColors.primaryColor,
              fontWeight: FontWeight.bold,
              fontSize: 14),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MogeColors.primaryColor),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MogeColors.primaryColor),
          ),
        ),
      ),
    );
  }
}
