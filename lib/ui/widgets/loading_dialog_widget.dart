import 'package:flutter/material.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class LoadingDialogWidget {
  static showLoading(BuildContext context, {String? message}) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset(
                    "assets/icons/loading.gif",
                    width: 60,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                    child: Text(
                      message ?? "Loading. . .",
                      style: TextStyle(
                        color: MogeColors.primaryColor,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
