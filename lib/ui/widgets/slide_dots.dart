import 'package:flutter/material.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class SlideDots extends StatelessWidget {
  final bool isActive;

  SlideDots(this.isActive);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 5),
      height: isActive ? 9 : 7,
      width: isActive ? 9 : 7,
      decoration: BoxDecoration(
        color: isActive ? MogeColors.primaryColor : Colors.grey,
        borderRadius: BorderRadius.all(
          Radius.circular(12),
        ),
      ),
    );
  }
}
