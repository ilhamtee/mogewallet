import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/utils/config/style_config.dart';
import 'package:moge_wallet/utils/services/dreams_service.dart';

class WidgetBottomSheet {
  void show(BuildContext context, DreamsModel data) {
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        builder: (context) {
          return Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () async {
                          await DreamsService.deleteDreams(data.id!);
                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Row(
                          children: <Widget>[
                            SvgPicture.asset(
                              "assets/icons/stop_hand.svg",
                              width: 40,
                              colorFilter: ColorFilter.mode(
                                MogeColors.primaryColor,
                                BlendMode.srcIn,
                              ),
                            ),
                            SizedBox(width: 20),
                            Text(
                              "Hentikan",
                              style: TextStyle(
                                  color: MogeColors.primaryColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w300),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 1,
                        color: MogeColors.primaryColor,
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                      ),
                      GestureDetector(
                        onTap: () async {
                          DreamsModel dataDreams = data;
                          if (data.isActive == "1") {
                            dataDreams.isActive = "0";
                          } else {
                            dataDreams.isActive = "1";
                          }

                          await DreamsService.updateDreams(
                              dataDreams.id!, dataDreams);

                          Navigator.pop(context);
                          Navigator.pop(context);
                        },
                        child: Row(
                          children: <Widget>[
                            data.isActive == "1"
                                ? SvgPicture.asset(
                                    "assets/icons/hold_hand.svg",
                                    width: 40,
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                  )
                                : SvgPicture.asset(
                                    "assets/icons/resume_button.svg",
                                    width: 40,
                                    colorFilter: ColorFilter.mode(
                                      MogeColors.primaryColor,
                                      BlendMode.srcIn,
                                    ),
                                  ),
                            SizedBox(width: 20),
                            Text(
                              data.isActive == "1" ? "Tunda" : "Lanjutkan",
                              style: TextStyle(
                                color: MogeColors.primaryColor,
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 20),
                    width: MediaQuery.of(context).size.width,
                    alignment: Alignment.center,
                    child: Text(
                      "Batal",
                      style: TextStyle(
                          color: MogeColors.primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }
}
