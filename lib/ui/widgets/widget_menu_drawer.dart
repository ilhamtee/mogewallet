import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class WidgetMenuDrawer extends StatelessWidget {
  final String icon;
  final String title;
  final VoidCallback onTap;

  WidgetMenuDrawer({
    required this.icon,
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: SvgPicture.asset(
        icon,
        width: 30,
        colorFilter: ColorFilter.mode(
          MogeColors.primaryColor,
          BlendMode.srcIn,
        ),
      ),
      title: Text(
        title,
        style: TextStyle(
            color: MogeColors.primaryColor,
            fontWeight: FontWeight.w300,
            fontSize: 20),
      ),
      onTap: onTap,
    );
  }
}
