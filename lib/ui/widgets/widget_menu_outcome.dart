import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:moge_wallet/utils/config/style_config.dart';

class WidgetMenuOutcome extends StatelessWidget {
  final String title;
  final String assets;

  WidgetMenuOutcome({
    required this.title,
    required this.assets,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SvgPicture.asset(
          assets,
          colorFilter: ColorFilter.mode(
            MogeColors.primaryColor,
            BlendMode.srcIn,
          ),
          width: 50,
        ),
        Text(
          title,
          style: TextStyle(
            color: MogeColors.primaryColor,
            fontSize: 16,
          ),
        )
      ],
    );
  }
}
