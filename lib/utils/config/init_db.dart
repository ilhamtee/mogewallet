import 'dart:io';
import 'dart:async';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class InitDB {
  const InitDB._();

  static Database? _database;

  static Future<Database> get database async {
    if (_database != null) return _database!;

    Directory directory = await getApplicationDocumentsDirectory();
    final path = join(directory.path, "moge.db");

    _database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute("""
          CREATE TABLE ${ConstantHelper.USER_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nama TEXT,
          no_handphone TEXT,
          email TEXT,
          password TEXT,
          isParent TEXT)""");

      await db.execute("""
          CREATE TABLE ${ConstantHelper.RULES_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          rulesName TEXT,
          date TEXT,
          nominal TEXT,
          isPaid TEXT)""");

      await db.execute("""
          CREATE TABLE ${ConstantHelper.CHAT_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          idUser TEXT,
          chatText TEXT,
          date TEXT,
          isParent TEXT)""");

      await db.execute("""
          CREATE TABLE ${ConstantHelper.INCOME_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nama TEXT,
          date TEXT,
          nominal TEXT,
          jenisRiwayat TEXT,
          type TEXT)""");

      await db.execute("""
          CREATE TABLE ${ConstantHelper.DREAMS_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          namaImpian TEXT,
          tanggalPencapaian TEXT,
          danaPencapaian TEXT,
          danaAwal TEXT,
          danaTerkumpul TEXT,
          tabunganOtomatis TEXT,
          nominalTabunganOtomatis TEXT,
          imageDummy TEXT,
          imagePath TEXT,
          isKunciTglPencapaian TEXT,
          isActive TEXT)""");

      await db.execute("""
          CREATE TABLE ${ConstantHelper.SALDO_MOMS_TABLE}(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          saldo TEXT)""");
    });

    return _database!;
  }
}
