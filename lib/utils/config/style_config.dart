import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MogeColors {
  static Color primaryColor = Color.fromRGBO(10, 8, 115, 1);
  static Color primaryDarkColor = Color.fromRGBO(3, 2, 82, 1);
  static Color bluish_grey = Color.fromRGBO(120, 144, 156, 1);
  static Color text_dark = Color.fromRGBO(38, 50, 56, 1);
  static Color pale_grey_two = Color.fromRGBO(241, 243, 245, 1);
  static Color pale_grey = Color.fromRGBO(236, 239, 241, 1);
  static Color cool_grey = Color.fromRGBO(176, 190, 197, 1);
  static Color redColor = Color.fromRGBO(209, 0, 0, 1);
  static Color lightGreenColor = Color.fromRGBO(48, 230, 48, 1);
  static Color lightBlueColor = Color.fromRGBO(232, 241, 255, 1);
  static Color darkBlueColor = Color.fromRGBO(197, 209, 227, 1);
  static Color drawerBackgroundColor = Color.fromRGBO(204, 223, 255, 0.5);

  static Color blueColor = Color.fromRGBO(19, 83, 191, 1);
  static Color statusBarColor = Color(0xFF00A8A6);
  static Color statusBarGreyColor = Color(0xFFF4F8F9);
  static Color lightBlueBaseColor = Color(0xffcdfaf9);
  static Color darkBlueBaseColor = Color(0xff017978);
  static Color yellowColor = Color(0xfffbd22c);
  static Color lightGreyColor = Color(0xffeceff1);
  static Color disableGreyColor = Color(0xfff1f3f5);
  static Color disableTextGreyColor = Color(0xffb0bec5);
  static Color darkGreyColor = Color(0xff78909c);
  static Color backgroundColor = Color(0xffF0F3FA);
  static Color searchColor = Color.fromRGBO(0, 158, 155, 1);
  static Color tableColorGreen = Color(0xFFE6F8F9);
  static Color tableColorPurple = Color(0xFFE4D8F7);
  static Color dividerColor = Color(0xFFECEFF1);
  static Color green100 = Color(0xFFDFF1D1);
  static Color green400 = Color(0xFF4A6933);
  static Color blue100 = Color(0xFFCCE4F3);
  static Color blue400 = Color(0xFF0077C5);
  static Color turq100 = Color(0xFFE6F8F9);
  static Color turq400 = Color(0xFF009E9B);
  static Color neutral100 = Color(0xFFF6F8FD);
  static Color neutral300 = Color(0xFFB0BEC5);
  static Color neutral400 = Color(0xFF78909C);
}
