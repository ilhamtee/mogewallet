class ConstantHelper {
  // Constant for Menu Drawer
  static const int INDEX_MENU_DASHBOARD = 0;
  static const int INDEX_MENU_INCOME = 1;
  static const int INDEX_MENU_OUTCOME = 2;
  static const int INDEX_MENU_MONEY_SAVING = 3;
  static const int INDEX_MENU_REMINDER = 4;
  static const int INDEX_MENU_MOMS_WALLET = 5;
  static const int INDEX_MENU_HISTORY = 6;
  static const int INDEX_MENU_SETTING = 7;

  // Constant for Table Database Name
  static const String USER_TABLE = "User";
  static const String RULES_TABLE = "Rules";
  static const String DREAMS_TABLE = "Dreams";
  static const String CHAT_TABLE = "Chat";
  static const String INCOME_TABLE = "Income";
  static const String SALDO_MOMS_TABLE = "SaldoMomsWallet";

  // Constant for Shared Preferences
  static const String ID_USER = "id_user";
  static const String USER_NAME = "username";
  static const String IS_LOGIN = "is_login";
  static const String CODE_REFERRAL = "code_referral";

  // Constant for font family
  static const String MEDIO_VINTAGE = "MedioVintage";
}
