import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';

class DateTimeHelper {
  static String dateFormatSeparator(date) {
    String format = formatDate(
        DateTime(int.parse(date.split("-")[0]), int.parse(date.split("-")[1]),
            int.parse(date.split("-")[2])),
        [dd, ' ', MM, ' ', yyyy]);
    return format;
  }

  static String dateParseLocalSeparator(String date) {
    DateTime dateTime = DateTime.parse(date);
    DateFormat formatter = DateFormat('dd MMM yyyy', "id");
    String formatted = formatter.format(dateTime);
    return formatted;
  }

  static String getTimeofDate(String date) {
    DateTime dateTime = DateTime.parse(date);
    DateFormat formatter = DateFormat('HH.mm');
    String formatted = formatter.format(dateTime);
    return formatted;
  }

  static String standardDateFormat(date) {
    DateTime dateTime = DateTime.parse(date);
    DateFormat formatter = DateFormat('yyyy-MM-dd', "id");
    String formatted = formatter.format(dateTime);
    return formatted;
  }

  static String getDaysFromCurrentDate(date) {
    String format = formatDate(
        DateTime(int.parse(date.split("-")[0]), int.parse(date.split("-")[1]),
            int.parse(date.split("-")[2])),
        [dd]);
    return format;
  }

  static String getDateFromCurrentDate() {
    String format = DateFormat("yyyy-MM-dd HH:mm").format(DateTime.now());
    return format;
  }

  static String parseDateWithTime(String date) {
    DateTime dateTime = DateTime.parse(date);
    String format = DateFormat("dd MMM yyyy \nHH:mm").format(dateTime);
    return format;
  }

  static String getMonthFromCurrentDate(date) {
    DateTime dateTime = DateTime.parse(date);
    DateFormat formatter = DateFormat('MMM');
    String formatted = formatter.format(dateTime);
    return formatted;
  }

  static String getYearFromCurrentDate(date) {
    String format = formatDate(
        DateTime(int.parse(date.split("-")[0]), int.parse(date.split("-")[1]),
            int.parse(date.split("-")[2])),
        [yyyy]);
    return format;
  }
}
