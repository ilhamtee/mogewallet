class GlobalMethodHelper {
  static bool isEmpty(text) {
    if (text == "" || text == null || text == "null") {
      return true;
    } else {
      return false;
    }
  }

  static bool isZeroValue(text) {
    if (text == "0" || text == "0.0" || text == 0 || text == 0.0) {
      return true;
    } else {
      return false;
    }
  }

  static String formatNumberToString(String text, {String defaultValue = "0"}) {
    if (GlobalMethodHelper.isEmpty(text)) {
      return defaultValue;
    }
    return int.parse(double.parse(text).toStringAsFixed(0)).toString();
  }

  static int formatTextNumberToInt(String text, {int defaultValue = 0}) {
    if (GlobalMethodHelper.isEmpty(text)) {
      return defaultValue;
    }
    return int.parse(double.parse(text).toStringAsFixed(0));
  }

  static bool isEmptyList(List<dynamic>? list) {
    if (list == null) {
      return true;
    } else if (list.length == 0) {
      return true;
    } else {
      return false;
    }
  }

  static String removeTrailingZero(String text) {
    ///NOTE: round to 3 number after comas
    String _roundthreedecimal = (double.parse(text)).toStringAsFixed(3);

    ///NOTE: add zero remover for 3.000 -> 3.0, 3.27000 -> 3.27, and round to 3 number after comas
    String _removeLongerDecimal = double.parse(_roundthreedecimal).toString();

    ///NOTE: remove zero after comas including its comas, ex: 3.0 -> 3, 2.2 -> 2.2
    return _removeLongerDecimal.replaceAll(RegExp(r"([.]*0)(?!.*\d)"), "");
  }

  static int validateNumber(String? valueInput) {
    int value = 0;
    if (valueInput != null && valueInput != "null" && valueInput != "") {
      value = int.parse(valueInput);
    }
    return value;
  }

  static double validateNumberDouble(String? valueInput) {
    double value = 0;
    if (valueInput != null && valueInput != "null" && valueInput != "") {
      value = double.parse(valueInput);
    }
    return value;
  }

  static bool validateBoolean(bool valueInput) {
    bool value = false;
    if (!isEmpty(valueInput.toString())) {
      value = valueInput;
    }
    return value;
  }
}
