import 'package:flutter/material.dart';

class NavigateHelpers {
  static pushAndRemove(BuildContext context, Widget pages) {
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => pages), (route) => false);
  }

  static push(BuildContext context, Widget pages) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => pages));
  }
}
