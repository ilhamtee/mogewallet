import 'dart:math';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:intl/number_symbols.dart';
import 'package:intl/number_symbols_data.dart';
import 'package:moge_wallet/utils/helper/global_method_helper.dart';

abstract class FormatNumber implements NumberFormat {
  static String formatter(String number) {
    final format = new NumberFormat("#,##0.###", "en-US");
    int? newNumber;
    try {
      newNumber = int.parse(GlobalMethodHelper.formatNumberToString(number));
    } catch (e) {
      print(e);
    }
    String value = format.format(newNumber);
    return value;
  }

  static String formatterLocal(String number) {
    final format = new NumberFormat("#,##0.###", "id");
    int? newNumber;
    try {
      newNumber = int.parse(GlobalMethodHelper.formatNumberToString(number));
    } catch (e) {
      print(e);
    }
    String value = format.format(newNumber);
    return value;
  }

  static String amountFormatter(String number) {
    numberFormatSymbols['spr'] = new NumberSymbols(
      NAME: "spr",
      DECIMAL_SEP: '.',
      GROUP_SEP: '.',
      PERCENT: '%',
      ZERO_DIGIT: '0',
      PLUS_SIGN: '+',
      MINUS_SIGN: '-',
      EXP_SYMBOL: 'e',
      PERMILL: '\u2030',
      INFINITY: '\u221E',
      NAN: 'NaN',
      DECIMAL_PATTERN: '#,##0.###',
      SCIENTIFIC_PATTERN: '#E0',
      PERCENT_PATTERN: '#,##0%',
      CURRENCY_PATTERN: '.#,##0.00',
      DEF_CURRENCY_CODE: 'AUD',
    );

    // TODO: implement format
    final format = new NumberFormat("###,##0", "spr");
    int? newNumber;
    try {
      newNumber = int.parse(number);
    } catch (e) {
      print(e);
    }
    String value = format.format(newNumber);
    return value;
  }

  static String stockFormatter(String number) {
    numberFormatSymbols['spr'] = new NumberSymbols(
      NAME: "spr",
      DECIMAL_SEP: '.',
      GROUP_SEP: ',',
      PERCENT: '%',
      ZERO_DIGIT: '0',
      PLUS_SIGN: '+',
      MINUS_SIGN: '-',
      EXP_SYMBOL: 'e',
      PERMILL: '\u2030',
      INFINITY: '\u221E',
      NAN: 'NaN',
      DECIMAL_PATTERN: '#,##0.###',
      SCIENTIFIC_PATTERN: '#E0',
      PERCENT_PATTERN: '#,##0%',
      CURRENCY_PATTERN: '.#,##0.00',
      DEF_CURRENCY_CODE: 'AUD',
    );

    // TODO: implement format
    final format = new NumberFormat("###,##0.###", "spr");
    double? newNumber;
    try {
      if (number.substring(0, 1) == "-") {
        newNumber = 0;
      } else {
        newNumber = double.parse(number);
      }
    } catch (e) {
      print(e);
    }
    String value = format.format(newNumber);
    return value;
  }
}

class CurrencyInputFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      print(true);
      return newValue;
    }

    double value = double.parse(newValue.text);

    final formatter = new NumberFormat("###,###", "id_ID");

    String newText = formatter.format(value);

    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}

class NoRekeningFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      print(true);
      return newValue;
    }
    List<String> separatedValue = [];
    String newText = "";
    separatedValue = newValue.text.split("");
    for (int i = 0; i < separatedValue.length; i++) {
      if (i % 4 == 0 && i != 0) {
        newText += "-";
      }
      newText += separatedValue[i];
    }
    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}

String resetCurrencyFormat(String text) {
  RegExp regexp = new RegExp(
    r"^|-|,| ",
    caseSensitive: false,
    multiLine: false,
  );

  String finalText = text.replaceAll(regexp, "");
  return finalText;
}

String encodeThousandsNominal(String nominal) {
  return FormatNumber.formatter(nominal.replaceAll(new RegExp(r"[^0-9]"), ""));
}

int decodeThousandsNominal(String formatted) {
  return int.parse(formatted.replaceAll(new RegExp(r"[^0-9]"), ""));
}

///
/// An implementation of [NumberInputFormatter] automatically inserts thousands
/// separators to numeric input. For example, a input of `1234` should be
/// formatted to `1,234`.
///
class ThousandsFormatter extends NumberInputFormatter {
  final FilteringTextInputFormatter _decimalFormatter;
  final String _decimalSeparator;
  final RegExp _decimalRegex;

  final NumberFormat formatter;
  final bool allowFraction;

  ThousandsFormatter({required this.formatter, this.allowFraction = false})
      : _decimalSeparator = (formatter).symbols.DECIMAL_SEP,
        _decimalRegex = RegExp(allowFraction
            ? '[0-9]+([${(formatter).symbols.DECIMAL_SEP}])?'
            : r'\d+'),
        _decimalFormatter = FilteringTextInputFormatter(
            RegExp(allowFraction
                ? '[0-9]+([${(formatter).symbols.DECIMAL_SEP}])?'
                : r'\d+'),
            allow: false);

  @override
  String _formatPattern(String? digits) {
    if (digits == null || digits.isEmpty) return digits ?? '';
    final number = allowFraction
        ? double.tryParse(digits) ?? 0.0
        : int.tryParse(digits) ?? 0;
    final result = (formatter).format(number);
    if (allowFraction && digits.endsWith(_decimalSeparator)) {
      return '$result$_decimalSeparator';
    }
    return result;
  }

  @override
  TextEditingValue _formatValue(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return _decimalFormatter.formatEditUpdate(oldValue, newValue);
  }

  @override
  bool _isUserInput(String s) {
    return s == _decimalSeparator || _decimalRegex.firstMatch(s) != null;
  }
}

///
/// An implementation of [NumberInputFormatter] that converts a numeric input
/// to credit card number form (4-digit grouping). For example, a input of
/// `12345678` should be formatted to `1234 5678`.
///
class CreditCardFormatter extends NumberInputFormatter {
  static final RegExp _digitOnlyRegex = RegExp(r'\d+');
  static final FilteringTextInputFormatter _digitOnlyFormatter =
      FilteringTextInputFormatter(_digitOnlyRegex, allow: false);

  final String separator;

  CreditCardFormatter({this.separator = ' '});

  @override
  String _formatPattern(String digits) {
    StringBuffer buffer = StringBuffer();
    int offset = 0;
    int count = min(4, digits.length);
    final length = digits.length;
    for (; count <= length; count += min(4, max(1, length - count))) {
      buffer.write(digits.substring(offset, count));
      if (count < length) {
        buffer.write(separator);
      }
      offset = count;
    }
    return buffer.toString();
  }

  @override
  TextEditingValue _formatValue(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return _digitOnlyFormatter.formatEditUpdate(oldValue, newValue);
  }

  @override
  bool _isUserInput(String s) {
    return _digitOnlyRegex.firstMatch(s) != null;
  }
}

///
/// An abstract class extends from [TextInputFormatter] and does numeric filter.
/// It has an abstract method `_format()` that lets its children override it to
/// format input displayed on [TextField]
///
abstract class NumberInputFormatter extends TextInputFormatter {
  late TextEditingValue _lastNewValue;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    /// nothing changes, nothing to do
    if (newValue.text == _lastNewValue.text) {
      return newValue;
    }
    _lastNewValue = newValue;

    /// remove all invalid characters
    newValue = _formatValue(oldValue, newValue);

    /// current selection
    int selectionIndex = newValue.selection.end;

    /// format original string, this step would add some separator
    /// characters to original string
    final newText = _formatPattern(newValue.text);

    /// count number of inserted character in new string
    int insertCount = 0;

    /// count number of original input character in new string
    int inputCount = 0;
    for (int i = 0; i < newText.length && inputCount < selectionIndex; i++) {
      final character = newText[i];
      if (_isUserInput(character)) {
        inputCount++;
      } else {
        insertCount++;
      }
    }

    /// adjust selection according to number of inserted characters staying before
    /// selection
    selectionIndex += insertCount;
    selectionIndex = min(selectionIndex, newText.length);

    /// if selection is right after an inserted character, it should be moved
    /// backward, this adjustment prevents an issue that user cannot delete
    /// characters when cursor stands right after inserted characters
    if (selectionIndex - 1 >= 0 &&
        selectionIndex - 1 < newText.length &&
        !_isUserInput(newText[selectionIndex - 1])) {
      selectionIndex--;
    }

    return newValue.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: selectionIndex),
        composing: TextRange.empty);
  }

  /// check character from user input or being inserted by pattern formatter
  bool _isUserInput(String s);

  /// format user input with pattern formatter
  String _formatPattern(String digits);

  /// validate user input
  TextEditingValue _formatValue(
      TextEditingValue oldValue, TextEditingValue newValue);
}
