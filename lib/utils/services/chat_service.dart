import 'dart:async';
import 'package:moge_wallet/models/chat_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class ChatService {
  ChatService._();

  static Future<List<ChatModel>> getAllChats() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.CHAT_TABLE);

    return List.generate(maps.length, (i) {
      return ChatModel(
          id: maps[i]['id'] as int,
          idUser: maps[i]['idUser'] as String,
          chatText: maps[i]['chatText'] as String,
          date: maps[i]['date'] as String,
          isParent: maps[i]['isParent'] as String);
    });
  }

  static Future<int> addChat(ChatModel chat) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.CHAT_TABLE,
      chat.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<int> deleteAllChat() async {
    final db = await InitDB.database;

    int result = await db.delete(
      ConstantHelper.CHAT_TABLE,
    );

    return result;
  }
}
