import 'dart:async';
import 'package:moge_wallet/models/dreams_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class DreamsService {
  DreamsService._();

  static Future<int> addDreams(DreamsModel dreams) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.DREAMS_TABLE,
      dreams.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<int> updateDreams(int id, DreamsModel dreams) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.DREAMS_TABLE, dreams.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<List<DreamsModel>> getDataDreams() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.DREAMS_TABLE);

    return List.generate(maps.length, (i) {
      return DreamsModel(
        id: maps[i]['id'] as int,
        namaImpian: maps[i]['namaImpian'] as String,
        tanggalPencapaian: maps[i]['tanggalPencapaian'] as String,
        danaPencapaian: maps[i]['danaPencapaian'] as String,
        danaAwal: maps[i]['danaAwal'] as String,
        danaTerkumpul: maps[i]['danaTerkumpul'] as String,
        tabunganOtomatis: maps[i]['tabunganOtomatis'] as String,
        nominalTabunganOtomatis: maps[i]['nominalTabunganOtomatis'] as String,
        imageDummy: maps[i]['imageDummy'] as String,
        imagePath: maps[i]['imagePath'] as String,
        isKunciTglPencapaian: maps[i]['isKunciTglPencapaian'] as String,
        isActive: maps[i]['isActive'] as String,
      );
    });
  }

  static Future<void> deleteDreams(int id) async {
    final db = await InitDB.database;

    await db.delete(
      ConstantHelper.DREAMS_TABLE,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
