import 'dart:async';
import 'package:moge_wallet/ui/pages/menu/history/model/history_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class IncomeService {
  IncomeService._();

  static Future<int> addIncome(HistoryModel data) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.INCOME_TABLE,
      data.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<List<HistoryModel>> getDataIncome() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.INCOME_TABLE);

    return List.generate(maps.length, (i) {
      return HistoryModel(
        id: maps[i]['id'] as int,
        nama: maps[i]['nama'] as String,
        date: maps[i]['date'] as String,
        nominal: maps[i]['nominal'] as String,
        jenisRiwayat: maps[i]['jenisRiwayat'] as String,
        type: maps[i]['type'] as String,
      );
    });
  }

  static Future<int> updateIncome(int id, HistoryModel data) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.INCOME_TABLE, data.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }
}
