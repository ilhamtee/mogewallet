import 'dart:async';
import 'package:moge_wallet/ui/pages/login/model/login_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';

class LoginService {
  LoginService._();

  static Future<List<LoginModel>> getAllUsers() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.USER_TABLE);

    return List.generate(maps.length, (i) {
      return LoginModel(
        id: maps[i]['id'] as int,
        nama: maps[i]['nama'] as String,
        no_handphone: maps[i]['no_handphone'] as String,
        email: maps[i]['email'] as String,
        password: maps[i]['password'] as String,
        isParent: maps[i]['isParent'] as String,
      );
    });
  }
}
