import 'dart:async';
import 'package:moge_wallet/ui/pages/register/model/register_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class RegisterService {
  static Future<int> addUser(RegisterModel user) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.USER_TABLE,
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<int> updateUser(int id, RegisterModel user) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.USER_TABLE, user.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }
}
