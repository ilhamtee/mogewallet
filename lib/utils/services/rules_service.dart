import 'dart:async';
import 'package:moge_wallet/ui/pages/menu/moms_wallet/model/rules_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class RulesService {
  RulesService._();

  static Future<int> addRules(RulesModel rules) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.RULES_TABLE,
      rules.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<int> updateRules(int id, RulesModel rules) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.RULES_TABLE, rules.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<List<RulesModel>> getDataRules() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.RULES_TABLE);

    return List.generate(maps.length, (i) {
      return RulesModel(
        id: maps[i]['id'] as int,
        rulesName: maps[i]['rulesName'] as String,
        date: maps[i]['date'] as String,
        nominal: maps[i]['nominal'] as String,
        isPaid: maps[i]['isPaid'] as String,
      );
    });
  }

  static Future<void> deleteRules(int id) async {
    final db = await InitDB.database;

    await db.delete(
      ConstantHelper.RULES_TABLE,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
