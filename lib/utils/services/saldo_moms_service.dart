import 'dart:async';
import 'package:moge_wallet/models/saldo_moms_wallet.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';
import 'package:sqflite/sqflite.dart';

class MomsBalanceService {
  MomsBalanceService._();

  static Future<int> addSaldo(SaldoMomsWalletModel saldo) async {
    final db = await InitDB.database;

    return db.insert(
      ConstantHelper.SALDO_MOMS_TABLE,
      saldo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  static Future<int> updateBalanceById(
      int id, SaldoMomsWalletModel saldo) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.SALDO_MOMS_TABLE, saldo.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }

  static Future<List<SaldoMomsWalletModel>> getAllBalances() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.SALDO_MOMS_TABLE);

    return List.generate(maps.length, (i) {
      return SaldoMomsWalletModel(
        id: maps[i]['id'] as int,
        saldo: maps[i]['saldo'] as String,
      );
    });
  }

  static Future<void> deleteBalanceById(int id) async {
    final db = await InitDB.database;

    await db.delete(
      ConstantHelper.SALDO_MOMS_TABLE,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}
