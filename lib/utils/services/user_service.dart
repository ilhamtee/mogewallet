import 'dart:async';
import 'package:moge_wallet/models/user_model.dart';
import 'package:moge_wallet/utils/config/init_db.dart';
import 'package:moge_wallet/utils/helper/constant_helper.dart';

class UserService {
  UserService._();

  static Future<List<UserModel>> getDataUser() async {
    final db = await InitDB.database;
    final maps = await db.query(ConstantHelper.USER_TABLE);

    return List.generate(maps.length, (i) {
      return UserModel(
        id: maps[i]['id'] as int,
        nama: maps[i]['nama'] as String,
        no_handphone: maps[i]['no_handphone'] as String,
        email: maps[i]['email'] as String,
        password: maps[i]['password'] as String,
        isParent: maps[i]['isParent'] as String,
      );
    });
  }

  static Future<int> updateUser(int id, UserModel user) async {
    final db = await InitDB.database;

    int result = await db.update(ConstantHelper.USER_TABLE, user.toMap(),
        where: "id = ?", whereArgs: [id]);
    return result;
  }
}
